//
//  trackHistoryViewController.h
//  HotRodTV0
//
//  Created by Raghee Chandran M on 4/12/16.
//  Copyright © 2016 Sieva Networks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>


@interface trackHistoryViewController : UIViewController <MKMapViewDelegate,MKAnnotation>{
    
    MKMapView *mapview1;
    NSMutableData *receivedData;
    
    NSString *filePath;
    NSString *str;
    //NSArray *myArray;
    
    BOOL connectionSuccess;
    int size1;
    
    
}
@property (strong, nonatomic) NSArray *myArray;

@property (weak, nonatomic) IBOutlet UISegmentedControl *mapcontrol;

@property (strong, nonatomic) IBOutlet MKMapView *mapview1;

- (IBAction)changeMapView:(id)sender;
@end
