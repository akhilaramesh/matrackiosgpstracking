//
//  TravelLogViewController.m
//  HotRodTV0
//
//  Created by Raghee Chandran M on 2/10/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//


#import "TravelLogViewController.h"
#import "DatePickerViewController.h"
#import "DriverListViewController.h"
#import "FirstViewController.h"
#import "FifthViewController.h"


@interface TravelLogViewController ()

@end

@implementation TravelLogViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    /*NSLog(@"Start:%@",start);
     NSLog(@"Start:%@",end);
     NSLog(@"Start:%@",path);*/
    NSLog(@"driver selected:%@",driver);
    //  NSLog(@"Start:%@",option);*/
    NSString *s = [driver stringByReplacingOccurrencesOfString:@" " withString:@""];
    if([option  isEqual:@"MOVEMENTS ONLY"]){option=@"MOVEMENT";}
    else if([option  isEqual:@"IGNITION ON/OFF"]){option=@"IGNITION";}
    else if([option  isEqual:@"STOPS ONLY"]){option=@"STOPSONLY";}
    
    NSString *urlString = [NSString stringWithFormat:@"%@/trackreport_b.php?driver=%@&startdate=%@&enddate=%@&select=%@",path,s,start,end,option];
    //NSString *urlString = @"%@/trackreport_b_new1.php?driver=%@&startdate=%@&enddate=%@&select=%@",path,driver,start,end,option;
    //NSLog(@"%@",urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [_travelLog loadRequest:urlRequest];
    
    //  _mywebview.delegate = self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
