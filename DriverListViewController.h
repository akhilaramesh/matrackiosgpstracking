//
//  DriverListViewController.h
//  HotRodTV0
//
//  Created by Raghee Chandran M on 2/8/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import <UIKit/UIKit.h>
extern NSString *driver;
@interface DriverListViewController : UIViewController <UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate>
{
    
    NSMutableData *receivedData;
    
    NSString *filePath;
    NSString *str;
    //NSArray *myArray;
    
    BOOL connectionSuccess;
    int size1;
    
}
@property (weak, nonatomic) IBOutlet UIView *pickerContainer;
@property (weak, nonatomic) IBOutlet UIPickerView *picker;

@property (weak, nonatomic) IBOutlet UITextField *output;
- (IBAction)DoneBtn:(id)sender;

@property (strong, nonatomic) NSArray *myArray;
@property (strong, nonatomic) NSMutableArray *myArray1;
@end
