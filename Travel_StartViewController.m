//
//  Travel_StartViewController.m
//  HotRodTV0
//
//  Created by Raghee Chandran M on 1/29/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import "Travel_StartViewController.h"
#import "FirstViewController.h"


@interface Travel_StartViewController ()

@end

@implementation Travel_StartViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *urlString1 = [NSString stringWithFormat:@"%@/trackhistory.html",path];
   // NSLog(@"Appended URL = %@",urlString1);
//    NSString *urlString = @"http://www.gpstracking-server1.com/gpstracking/client/demo/maps/trackhistory.html";
    NSURL *url = [NSURL URLWithString:urlString1];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [_Travelstart loadRequest:urlRequest];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
