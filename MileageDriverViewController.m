//
//  MileageDriverViewController.m
//  HotRodTV0
//
//  Created by Raghee Chandran M on 2/12/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import "MileageDriverViewController.h"
#import "FirstViewController.h"
#import "TrackDataViewController.h"
#import "MilegeViewController.h"

@interface MileageDriverViewController ()

@end
NSString *driver_mileage=@"";
int dr3=0;
@implementation MileageDriverViewController
@synthesize myArray;
@synthesize picker_Mileage;
@synthesize myArray1;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    _MileageContainer.hidden = YES;
    
    picker_Mileage.hidden = YES;
    
    _Dname_Mileage.delegate = self;
    // NSLog(@"array%@",driverlist3);
    myArray1 = [[NSMutableArray alloc]initWithArray:driver_name];
    _Dname_Mileage.text=[NSString stringWithFormat:@"%@",myArray1[0]];
    driver_mileage=[NSString stringWithFormat:@"%@",myArray1[0]];
    NSString *last=@"none";
    [myArray1 addObject:last];
    //  NSLog(@"Array:%@",myArray1);
    size1=(int)[myArray1 count];
    // NSLog(@"%d",size1);   // _content_Track = [[NSArray alloc] initWithObjects:@"ALL",@"STOPS ONLY",@"IGNITION ON/OFF",@"MOVEMENTS ONLY",nil];
    // CatPicker.hidden = NO;
    picker_Mileage.delegate=self;
    picker_Mileage.dataSource=self;
    [_Dname_Mileage setInputView:picker_Mileage];
    [picker_Mileage reloadAllComponents];
    
    
    //self.myArray =[[NSArray alloc]initWithObjects:@"red", nil];
    
    // Do any additional setup after loading the view.
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    // _category.inputView=CatPicker;
    
    
    _MileageContainer.hidden = NO;
    
    picker_Mileage.hidden = NO;
    
    
    
    
    
    
    
    return NO;
    
}




- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
    
    
}


//  returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return size1;
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return [myArray1 objectAtIndex:row];
    
    
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)componen1t{
    NSString *name  = @"none";
    if (myArray1 != nil && myArray1.count>0) {
        name =[myArray1 objectAtIndex:row];
    }
    _Dname_Mileage.text = name;
    [self driverSelected];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)DoneBtn_Mileage:(id)sender {
    
    [self driverSelected];
    _MileageContainer.hidden = YES;
    
    
}

-(void)driverSelected{
    NSString *driverName = _Dname_Mileage.text;
    if ([driverName  isEqualToString:@"none"]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Driver not exists-Please select another driver!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        _Dname_Mileage.text=driverName;
        driver_mileage = driverName;
    }
}

- (IBAction)nextAction:(id)sender {
    NSString *name = _Dname_Mileage.text;
    if([name isEqualToString:@""] || [name isEqualToString:@"none"] ){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Driver not exists-Please select another driver!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else{
        UIStoryboard *storyBoard = self.storyboard;
        UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"MileageViewViewController"];
        [self.navigationController pushViewController:vc animated:true];
    }
}
@end
