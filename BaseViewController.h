//
//  BaseViewController.h
//  MaTrack
//
//  Created by Kushal choudhari on 10/01/18.
//  Copyright © 2018 Sieva Networks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController
-(void)addNotification:(UIScrollView*)bgScroll;
@end
