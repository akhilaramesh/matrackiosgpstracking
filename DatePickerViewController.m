//
//  DatePickerViewController.m
//  HotRodTV0
//
//  Created by Raghee Chandran M on 2/10/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//


#import "DatePickerViewController.h"
#import "CommanDataHandler.h"
@interface DatePickerViewController ()

@end

NSString *start=@"";
NSString *end=@"";
NSDate *now;
int ixt=0;
int iyt=0;
@implementation DatePickerViewController



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    now = [NSDate date];
    NSDateFormatter *nowformatter = [[NSDateFormatter alloc]init];
    [nowformatter setDateFormat:@"MM/dd/YYYY"];
    NSString *now1=[nowformatter stringFromDate:now];
    _StartDate.text=[NSString stringWithFormat:@"%@",now1];
    start = [NSString stringWithFormat:@"%@",now1];
    end= [NSString stringWithFormat:@"%@",now1];
    _EndDate.text=[NSString stringWithFormat:@"%@",now1];
    _StartDate.delegate = self;
    datepicker=[[UIDatePicker alloc] init];
    [datepicker setMaximumDate:now];
    
    datepicker.datePickerMode=UIDatePickerModeDate;
    [datepicker addTarget:self action:@selector(ShowSelectedDate) forControlEvents:UIControlEventValueChanged];
    _StartDate.inputView = datepicker;
    // _Enddate.inputView = datepicker;
    
    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,230,44)];
    [toolBar setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(donePressed)];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space, doneBtn,nil]];
    [self.StartDate setInputAccessoryView:toolBar];
    
    
    
    _EndDate.delegate = self;
    datepicker1=[[UIDatePicker alloc] init];
    [datepicker1 setMinimumDate:datepicker.date];
    [datepicker1 setMaximumDate:now];
    datepicker1.datePickerMode=UIDatePickerModeDate;
    [datepicker1 addTarget:self action:@selector(ShowSelectedDate1) forControlEvents:UIControlEventValueChanged];
    _EndDate.inputView = datepicker1;
    
    UIToolbar *toolBar1=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,230,44)];
    [toolBar1 setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn1=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(donePressed1)];
    UIBarButtonItem *space1=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar1 setItems:[NSArray arrayWithObjects:space1, doneBtn1,nil]];
    [self.EndDate setInputAccessoryView:toolBar1];
    // Do any additional setup after loading the view.
}
- (void)ShowSelectedDate
{
    [self startDateSelected:false];
}
- (void)donePressed{
    [self startDateSelected:true];
    [self.view endEditing:true];
}

- (void)ShowSelectedDate1
{
    [self endDateSelected:false];
}
- (void)donePressed1{
    [self endDateSelected:true];
    [self.view endEditing:true];
    
}

-(void)startDateSelected:(BOOL)done{
    NSString *str = [[CommanDataHandler sharedInstance] startDateSelected:datepicker.date endDate:datepicker1.date isdone:done];
    if (![str isEqualToString:@""]) {
        [datepicker1 setMinimumDate:datepicker.date];
        self.StartDate.text= str;
        start = str;
    }else{
        datepicker.date = [CommanDataHandler getDateFromString:self.StartDate.text];
    }
    
}

-(void)endDateSelected:(BOOL)done{
    NSString*endDate= [[CommanDataHandler sharedInstance] endDateSelected:datepicker.date endDate:datepicker1.date isDone:done];
    if (![endDate isEqualToString:@""]) {
        [datepicker setMaximumDate:datepicker1.date];
        self.EndDate.text=endDate;
        end =endDate;
    }
    else
        datepicker1.date = [CommanDataHandler getDateFromString:self.EndDate.text];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
