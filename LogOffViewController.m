//
//  LogOffViewController.m
//  HotRodTV0
//
//  Created by Raghee Chandran M on 2/2/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import "LogOffViewController.h"

@interface LogOffViewController ()

@end

@implementation LogOffViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _retryButton.hidden = YES;
    _Logoff.hidden =YES;
    [self logoutAPI];
    
}
-(void)logoutAPI{
    NSString *urlString = [NSString stringWithFormat:@"%@/gpstracking/client_login_mobile.php?logoff",common_path];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [_Logoff loadRequest:urlRequest];
    _Logoff.delegate = self;
}

-(void)redirect{
    
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"initial"];
    [self presentViewController:vc animated:YES completion:nil];
    // [timer invalidate];
    
    
}
- (void)webViewDidFinishLoad:(UIWebView *)Logoff
{
    _retryButton.hidden =YES;
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"initial"];
    [self presentViewController:vc animated:YES completion:nil];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    _retryButton.hidden =NO;
    NSString *val= [error.userInfo valueForKey:NSLocalizedDescriptionKey];
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:val delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (IBAction)retryAction:(id)sender {
    [self logoutAPI];
}

@end
