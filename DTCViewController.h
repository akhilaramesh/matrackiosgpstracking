//
//  DTCViewController.h
//  HotRodTV0
//
//  Created by Raghee Chandran M on 1/29/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommanDataHandler.h"
extern NSString *start_dtc;
extern NSString *end_dtc;
@interface DTCViewController : UIViewController <UITextFieldDelegate>
{
    
    UIDatePicker *datepicker_DTC;
    UIDatePicker *datepicker1_DTC;
    
}

@property (weak, nonatomic) IBOutlet UITextField *StartDate_DTC;
@property (weak, nonatomic) IBOutlet UITextField *EndDate_DTC;

@end
