//
//  DTCViewViewController.m
//  HotRodTV0
//
//  Created by Raghee Chandran M on 2/12/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//


#import "DTCViewViewController.h"
#import "FirstViewController.h"
#import "DTCViewController.h"
#import "DTCDriverViewController.h"



@interface DTCViewViewController ()

@end

@implementation DTCViewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *s = [driver_dtc stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *urlString = [NSString stringWithFormat:@"%@/dtc_report_b.php?driver=%@&startdate=%@&enddate=%@",path,s,start_dtc,end_dtc];
    //NSString *urlString = @"%@/trackreport_b_new1.php?driver=%@&startdate=%@&enddate=%@&select=%@",path,driver,start,end,option;
    // NSLog(@"%@",urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [_DTCView loadRequest:urlRequest];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
