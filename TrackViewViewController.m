//
//  TrackViewViewController.m
//  HotRodTV0
//
//  Created by Raghee Chandran M on 2/11/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import "TrackViewViewController.h"
#import "SecondViewController.h"
#import "TrackDataViewController.h"
#import "TrackDriverViewController.h"
#import "FirstViewController.h"


@interface TrackViewViewController ()

@end

@implementation TrackViewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"Start:%@",start_Track);
    NSLog(@"Start:%@",end_Track);
    NSLog(@"Start:%@",path);
    NSLog(@"Start:%@",driver_Track);
    NSLog(@"Start:%@",option_Track);
    //NSString *s = [driver_Track stringByReplacingOccurrencesOfString:@" " withString:@""];
    /*  NSString *urlString = [NSString stringWithFormat:@"%@/trackhistory_2_app.php]",path];
     NSString *requestString = [NSString stringWithFormat:@"driver=%@&startdate=%@&enddate=%@&select=%@",driver_Track,start_Track,end_Track,option_Track];
     //NSString *urlString = @"%@/trackreport_b_new1.php?driver=%@&startdate=%@&enddate=%@&select=%@",path,driver,start,end,option;
     // NSLog(@"%@",urlString);
     NSURL *url = [NSURL URLWithString:urlString];
     NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
     [_TrackHistory loadRequest:urlRequest];*/
    // Do any additional setup after loading the view.*/*/
    /*NSData *requestdata = [NSData dataWithBytes:[requestString UTF8String] length:[requestString length]];
     
     NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
     [request setHTTPMethod:@"POST"];
     [request setValue:@"applocation/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
     [request setHTTPBody:requestdata];
     NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
     NSString *response = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
     NSLog(@"%@",response);*/
    if([option_Track  isEqual:@"MOVEMENTS ONLY"]){option_Track=@"MOVEMENT";}
    else if([option_Track  isEqual:@"IGNITION ON/OFF"]){option_Track=@"IGNITION";}
    else if([option_Track  isEqual:@"STOPS ONLY"]){option_Track=@"STOPSONLY";}
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@/trackhistory_2_app.php?driver=%@&startdate=%@&enddate=%@&select=%@",path,driver_Track,start_Track,end_Track,option_Track];
    NSURLRequest *theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:(60.0)];
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    if (theConnection) {
        receivedData = [NSMutableData data];
        NSString *receivedDataString = [[NSString alloc]initWithData:receivedData encoding:NSUTF8StringEncoding];
        //NSLog(@"This1: %@",receivedData);
        
        NSLog(@"This2: %@",receivedDataString);
        
    }else
    {
        
        //NSLog(@"Connection Failed");
    }
    
    
    
    
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    [receivedData setLength:0];
    NSLog(@"This3 :%@",receivedData);
    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    [receivedData appendData:data];
    filePath = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    
    connectionSuccess = YES;
    
    // NSLog(@"This4 :%@",[filePath stringByReplacingOccurrencesOfString:@"-" withString:@""]);
    NSLog(@"This5 ---->test--->:%@",receivedData);
    //NSLog(@"%@",filePath);
    /*filePath = [filePath stringByReplacingOccurrencesOfString:@"[" withString:@""];
     filePath = [filePath stringByReplacingOccurrencesOfString:@"]" withString:@""];
     //NSLog(@"%@",filePath);
     self.myArray = [filePath componentsSeparatedByString:@","];
     // int sizea = [myArray count];
     //[self getData];
     driverlist_cmn = [[NSMutableArray alloc]init];
     for(s3 in myArray)
     {
     
     s3 = [s3 stringByReplacingOccurrencesOfString:@"\"" withString:@""];
     [driverlist_cmn addObject:s3];
     
     }*/
    //driverlist = [[NSMutableArray alloc]initWithArray:myArray];
    // NSLog(@"driverarray------------>%@",driverlist_cmn);
    
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
