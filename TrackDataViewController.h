//
//  TrackDataViewController.h
//  HotRodTV0
//
//  Created by Raghee Chandran M on 2/11/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//


#import <UIKit/UIKit.h>
extern NSString *option_Track;
extern NSMutableArray *driverlist;
@interface TrackDataViewController : UIViewController <UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate>
{
    NSMutableData *receivedData;
    
    NSString *filePath;
    NSString *str;
    //NSArray *myArray;
    
    BOOL connectionSuccess;
    int size1;
    
    
}

- (IBAction)Done_Track:(id)sender;

@property (weak, nonatomic) IBOutlet UIPickerView *Picker_Track;

@property (weak, nonatomic) IBOutlet UIView *PickerViewContainer_Track;
@property (weak, nonatomic) IBOutlet UITextField *Category_Track;
@property (strong, nonatomic) NSArray *myArray;
@property (strong, nonatomic) NSArray *content_Track;
@property (strong, nonatomic) NSString *s;
@end
