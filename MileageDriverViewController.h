//
//  MileageDriverViewController.h
//  HotRodTV0
//
//  Created by Raghee Chandran M on 2/12/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import <UIKit/UIKit.h>
extern NSString *driver_mileage;
@interface MileageDriverViewController : UIViewController <UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate>
{
    
    NSMutableData *receivedData;
    
    NSString *filePath;
    NSString *str;
    //NSArray *myArray;
    
    BOOL connectionSuccess;
    int size1;
    
}
@property (weak, nonatomic) IBOutlet UIView *MileageContainer;

@property (weak, nonatomic) IBOutlet UIPickerView *picker_Mileage;
- (IBAction)DoneBtn_Mileage:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *Dname_Mileage;

@property (strong, nonatomic) NSArray *myArray;
@property (strong, nonatomic) NSMutableArray *myArray1;


@end