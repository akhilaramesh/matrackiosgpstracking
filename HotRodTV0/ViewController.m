//
//  ViewController.m
//  HotRodTV0
//
//  Created by Raghee Chandran M on 1/28/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
   [_logImage setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    [_view1 setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    _logImage.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    _view1.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    _logImage.contentMode = UIViewContentModeScaleAspectFill;
    _view1.contentMode = UIViewContentModeScaleAspectFill;
    
	// Do any additional setup after loading the view, typically from a nib.
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    
    
    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
    
}

-(BOOL)wantsFullScreenLayout{
    
    return YES;
}


/*- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    CGRect appFrame;
    appFrame.origin = CGPointMake(0.0f, 0.0f);
    if((toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft) || (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight)){
        appFrame.size = CGSizeMake(480.0f, 300.0f);
        
        
    }
    else
    {
        
        appFrame.size = CGSizeMake(320.0f, 460.0f);
        
    }
    [conte]
    
}*/
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
