//
//  ViewController.h
//  HotRodTV0
//
//  Created by Raghee Chandran M on 1/28/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *logImage;
@property (weak, nonatomic) IBOutlet UIView *view1;

@end
