//
//  AppDelegate.h
//  HotRodTV0
//
//  Created by Raghee Chandran M on 1/28/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@end
