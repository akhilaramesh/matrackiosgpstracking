//
//  TravelLogViewController.h
//  HotRodTV0
//
//  Created by Raghee Chandran M on 2/10/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface TravelLogViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *travelLog;

@end
