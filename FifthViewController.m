//
//  FifthViewController.m
//  HotRodTV0
//
//  Created by Raghee Chandran M on 1/29/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import "FifthViewController.h"
#import "FirstViewController.h"

@interface FifthViewController ()

@end
NSString *option=@"";
NSString *s=@"";
NSMutableArray *driverlist1;
int opt1=0;
@implementation FifthViewController

@synthesize content;
@synthesize CatPicker;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _CatPickContainer.hidden = YES;
    
    CatPicker.hidden = YES;
    
    _category.delegate =self;
    _category.text=@"ALL";
    option=@"ALL";
    
    content= [[NSArray alloc] initWithObjects:@"ALL",@"STOPS ONLY",@"IGNITION ON/OFF",@"MOVEMENTS ONLY",nil];
    // CatPicker.hidden = NO;
    CatPicker.delegate=self;
    CatPicker.dataSource=self;
    [_category setInputView:CatPicker];
    [CatPicker reloadAllComponents];
    // NSLog(@"testing--------------------->%d",opt1);
    [CatPicker selectRow:0 inComponent:0 animated:YES];
    
    NSString *urlString = [NSString stringWithFormat:@"%@/driver_display.php",path];
    NSURLRequest *theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:(60.0)];
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    if (theConnection) {
        receivedData = [NSMutableData data];
        NSString *receivedDataString = [[NSString alloc]initWithData:receivedData encoding:NSUTF8StringEncoding];
        //NSLog(@"This1: %@",receivedData);
        
        NSLog(@"This2: %@",receivedDataString);
        
    }else
    {
        
        // NSLog(@"Connection Failed");
    }
    
    
    
}



- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    [receivedData setLength:0];
    // NSLog(@"This3 :%@",receivedData);
    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    [receivedData appendData:data];
    filePath = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    
    connectionSuccess = YES;
    
    //NSLog(@"This4 :%@",[filePath stringByReplacingOccurrencesOfString:@"-" withString:@""]);
    //NSLog(@"This5 ---->test--->:%@",receivedData);
    // NSLog(@"%@",filePath);
    filePath = [filePath stringByReplacingOccurrencesOfString:@"[" withString:@""];
    filePath = [filePath stringByReplacingOccurrencesOfString:@"]" withString:@""];
    // NSLog(@"%@",filePath);
    self.myArray = [filePath componentsSeparatedByString:@","];
    // int sizea = [myArray count];
    //[self getData];
    driverlist1 = [[NSMutableArray alloc]init];
    for(s in _myArray)
    {
        
        s = [s stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        [driverlist1 addObject:s];
        
    }
    //driverlist = [[NSMutableArray alloc]initWithArray:myArray];
    // NSLog(@"driverarray------------>%@",driverlist1);
    
}



- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    // _category.inputView=CatPicker;
    
    
    _CatPickContainer.hidden = NO;
    
    CatPicker.hidden = NO;
    
    
    
    
    
    
    
    return NO;
    
}




- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
    
    
}


//  returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return 4;
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return [content objectAtIndex:row];
    
    
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    opt1 = opt1 +1;
    NSInteger row1=[CatPicker selectedRowInComponent:0];
    str = [content objectAtIndex:row1];
    
    //_output.text =str;
    //  NSLog(@"selected%@",str);
    //NSLog(@"testing2-------->%d",opt1);
    
    
}
//[_StartDate.setInputAccessoryView:toolBar];
// _StartDate.inputAccessoryView = toolBar;
//_Enddate.inputAccessoryView = toolBar;*/




//NSString *urlString1 = [NSString stringWithFormat:@"%@/trackreport.html",path];
// NSLog(@"Appended URL = %@",urlString1);

//NSString *urlString = @"http://www.gpstracking-server1.com/gpstracking/client/demo/maps/trackreport.html";
// NSURL *url = [NSURL URLWithString:urlString1];
//  NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
// [_Travellog loadRequest:urlRequest];    // Do any additional setup after loading the view.


/*- (BOOL)textFieldShouldBeginediting:(UITextField *)textfield
 {
 datepicker.datePickerMode = UIDatePickerModeDate;
 textfield.inputView = datepicker;
 textfieldTag = textfield.tag;
 
 
 return YES;
 
 }*/
/*- (void)ShowSelectedDate
 {
 
 NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
 [formatter setDateFormat:@"dd/MM/yyyy"];
 
 self.StartDate.text=[NSString stringWithFormat:@"%@",[formatter stringFromDate:datepicker.date]];
 [self.StartDate resignFirstResponder];
 
 
 
 
 }
 - (void)ShowSelectedDate1
 {
 
 NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
 [formatter setDateFormat:@"dd/MM/yyyy"];
 self.Enddate.text=[NSString stringWithFormat:@"%@",[formatter stringFromDate:datepicker1.date]];
 [self.Enddate resignFirstResponder];
 
 
 
 }*/
/*- (void)donePressed
 {
 datepicker.hidden = YES;
 
 }*/

/*- (void)taskDatePicked:(id)sender
 {
 UITextField *textfield = (UITextField *)[self.view viewWithTag:textfieldTag];
 NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
 dateformatter.dateStyle = NSDateFormatterMediumStyle;
 
 [dateformatter setDateFormat:@"dd-MM-YYYY"];
 textfield.text = [dateformatter stringFromDate:[datepicker date]];
 
 
 }*/


/*- (void)ShowSelectedDate
 {
 NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
 [formatter setDateFormat:@"dd-MM-YYYY"];
 
 
 if ([_StartDate isFirstResponder]) {
 
 _StartDate.text=[NSString stringWithFormat:@"%@",[formatter stringFromDate:datepicker.date]];    }
 else if ([_Enddate isFirstResponder]){
 
 _Enddate.text=[NSString stringWithFormat:@"%@",[formatter stringFromDate:datepicker.date]];    }
 
 // self.StartDate.text=[NSString stringWithFormat:@"%@",[formatter stringFromDate:datepicker.date]];
 //[self.StartDate resignFirstResponder];
 
 // self.Enddate.text=[NSString stringWithFormat:@"%@",[formatter stringFromDate:datepicker.date]];
 //[self.Enddate resignFirstResponder];
 
 
 
 }*/




/*- (void)didReceiveMemoryWarning
 {
 [super didReceiveMemoryWarning];
 // Dispose of any resources that can be recreated.
 }*/

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


/*- (IBAction)textFieldEditing:(UITextField *)sender {
 
 datepicker=[[UIDatePicker alloc]init];
 datepicker.datePickerMode=UIDatePickerModeDate;
 sender.inputView = datepicker;
 datepicker addTarget:self action:@selector(@"") forControlEvents:<#(UIControlEvents)#>
 
 
 
 }
 
 - (IBAction)EndDateEdit:(UITextField *)sender {
 }*/


- (IBAction)Done:(id)sender {
    if (opt1 == 0) {
        
        _category.text=[content objectAtIndex:0];
        option =[content objectAtIndex:0];
        _CatPickContainer.hidden = YES;
    }
    else{
        
        _category.text=str;
        option = str;
        _CatPickContainer.hidden = YES;
        
    }
    
    
}
@end
