//
//  ThirdViewController.h
//  HotRodTV0
//
//  Created by Raghee Chandran M on 1/29/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import <UIKit/UIKit.h>
extern NSMutableArray *driverlist_cmn;
@interface ThirdViewController : UIViewController
{
    NSMutableData *receivedData;
    
    NSString *filePath;
    NSString *str;
    //NSArray *myArray;
    
    BOOL connectionSuccess;
    int size1;
    
    
}
@property (weak, nonatomic) IBOutlet UIButton *travellogBtn;
@property (weak, nonatomic) IBOutlet UIButton *mileageBtn;
@property (weak, nonatomic) IBOutlet UIButton *dtcBtn;
@property (weak, nonatomic) IBOutlet UIButton *fuelBtn;
@property (weak, nonatomic) IBOutlet UIButton *driverSafetyButn;
@property (strong, nonatomic) NSArray *myArray;
@property (strong, nonatomic) NSMutableArray *myArray1;@end

