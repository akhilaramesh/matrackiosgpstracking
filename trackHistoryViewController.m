//
//  trackHistoryViewController.m
//  HotRodTV0
//
//  Created by Raghee Chandran M on 4/12/16.
//  Copyright © 2016 Sieva Networks. All rights reserved.
//

#import "trackHistoryViewController.h"
#import "TrackDataViewController.h"
#import "FirstViewController.h"
#import  "TrackDriverViewController.h"
#import "SecondViewController.h"
#import "MapPin.h"
#import <CoreLocation/CoreLocation.h>

@interface trackHistoryViewController ()

@end
NSString *adress=@"";


@implementation trackHistoryViewController

@synthesize mapview1;
@synthesize myArray;

NSMutableArray *latitude1;
NSMutableArray *longitude1;
NSMutableArray *speed ;
NSMutableArray *date ;

NSMutableArray *time_map;
NSMutableArray *address;
NSMutableArray *annotations;

//NSString *str = [[NSString alloc]init];
- (void)viewDidLoad {
    [super viewDidLoad];
    mapview1.delegate = self;
    //NSArray *lat = [[NSArray alloc] init];
    //NSArray *lng =[[NSArray alloc] init];
    NSLog(@"Track History");
    NSString *dri=[driver_Track stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"driver%@",dri);
    annotations = [[NSMutableArray alloc] init];
    /*
     NSString *urlString = [NSString stringWithFormat:@"%@/trackhistory_2_app.php",path];
     NSLog(@"%@",urlString);
     NSString *requestString = [NSString stringWithFormat:@"driver=%@&startdate=%@&enddate=%@&select=%@",dri,start_Track,end_Track,option_Track];
     //NSString *urlString = @"%@/trackreport_b_new1.php?driver=%@&startdate=%@&enddate=%@&select=%@",path,driver,start,end,option;
     // NSLog(@"%@",urlString);
     //NSURL *url = [NSURL URLWithString:urlString];
     //NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
     //[_TrackHistory loadRequest:urlRequest];*/
    // Do any additional setup after loading the view.*/*/
    /*   NSData *myrequestdata = [NSData dataWithBytes:[requestString UTF8String] length:[requestString length]];
     
     NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
     [request setHTTPMethod:@"POST"];
     [request setValue:@"applocation/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
     [request setHTTPBody:myrequestdata];
     NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
     NSString *response = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
     NSLog(@"%@",response);*/
    
    
    
    
    
    // Do any additional setup after loading the view.
    //dri = @"driver1";
    NSLog(@"client:%@",usr);
    if([option_Track  isEqual:@"MOVEMENTS ONLY"]){option_Track=@"MOVEMENT";}
    else if([option_Track  isEqual:@"IGNITION ON/OFF"]){option_Track=@"IGNITION";}
    else if([option_Track  isEqual:@"STOPS ONLY"]){option_Track=@"STOPSONLY";}
    NSString *urlString = [NSString stringWithFormat:@"%@/gpstracking/trackhistory_2_app.php?client=%@&driver=%@&startdate=%@&enddate=%@&select=%@",common_path,usr,dri,start_Track,end_Track,option_Track];
    NSLog(@"%@",urlString);
    NSURLRequest *theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:(60.0)];
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    if (theConnection) {
        receivedData = [NSMutableData data];
        NSString *receivedDataString = [[NSString alloc]initWithData:receivedData encoding:NSUTF8StringEncoding];
        NSLog(@"This1: %@",receivedData);
        
        NSLog(@"This2: %@",receivedDataString);
        
    }else
    {
        
        //NSLog(@"Connection Failed");
    }
    
    
    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    [receivedData setLength:0];
    NSLog(@"This3 :%@",receivedData);
    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    [receivedData appendData:data];
    filePath = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    
    connectionSuccess = YES;
    
    NSLog(@"received:%@",filePath);
    // filePath = [filePath stringByReplacingOccurrencesOfString:@"[" withString:@""];
    //filePath = [filePath stringByReplacingOccurrencesOfString:@"]" withString:@""];
    
    self.myArray = [filePath componentsSeparatedByString:@","];
    NSLog(@"myarray:%lu",(unsigned long)[myArray count]);
    date = [[NSMutableArray alloc]init];

    speed = [[NSMutableArray alloc]init];
    time_map = [[NSMutableArray alloc] init];
    latitude1 = [[NSMutableArray alloc] init];
    longitude1 = [[NSMutableArray alloc] init];
    address = [[NSMutableArray alloc] init];   //NSLog(@"first item:%@",myArray[1]);
    int size2=(int)[myArray count];
    if ([myArray count] == 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention!!!!"
                                                        message:@"No data available for this time period" delegate:self cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else{
        NSLog(@"size%d:",size2);
        for(int i=1;i<size2-1;i++){
            NSArray *parts = [[NSArray alloc] init];
            
            parts = [myArray[i] componentsSeparatedByString:@"$"];
            NSLog(@"%@%@",parts[5],parts[9]);
            NSString *date1 = [parts[0] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
            [date addObject:[date1 stringByReplacingOccurrencesOfString:@"\\" withString:@""]];
            //[latitude insertObject:parts[2] atIndex:i];
            //[longitude insertObject:parts[3] atIndex:i];
            [latitude1 addObject:parts[1]];
            [longitude1 addObject:parts[2]];
            [speed addObject:parts[8]];
            [time_map addObject:parts[5]];
            [address addObject:parts[11]];
        }
        //[lan insertObject:lan_parts[1] atIndex:1];
        //[lng insertObject:parts[3] atIndex:i];
        //}
        /*// int sizea = [myArray count];
         //[self getData];
         driverlist_cmn = [[NSMutableArray alloc]init];
         for(s3 in myArray)
         {
         
         s3 = [s3 stringByReplacingOccurrencesOfString:@"\"" withString:@""];
         [driverlist_cmn addObject:s3];
         
         }
         //driverlist = [[NSMutableArray alloc]initWithArray:myArray];
         // NSLog(@"driverarray------------>%@",driverlist_cmn);
         
         }*/
        //[lan addObject:@"empty"];
        NSLog(@"latitude Array:%@:",latitude1);
        NSLog(@"longitude %@",longitude1);
        NSLog(@"Speed %@",speed);
        [self plotTrack];
    }
    
}
- (void)plotTrack {
    double lat1 = [latitude1[0] doubleValue];
    double lng1 = [longitude1[0] doubleValue];
    
    int size11 = (int)[latitude1 count];
    double lat2 = [latitude1[size11-1] doubleValue];
    double lng2 = [longitude1[size11-1] doubleValue];
    
    NSLog(@"last points:%f%f",lat2,lng2);
    
    CLLocationCoordinate2D center;
    center.latitude = lat1;
    center.longitude = lng1;
    
    CLLocationCoordinate2D dest;
    dest.latitude = lat2;
    dest.longitude = lng2;
    
    MKCoordinateSpan zoom;
    zoom.latitudeDelta=0.2;
    zoom.longitudeDelta=0.2;
    
    MKCoordinateRegion myregion = [mapview1 regionThatFits:MKCoordinateRegionMakeWithDistance(center, 2000, 2000)];
    myregion.center = center;
    myregion.span = zoom;
    [mapview1 setRegion:myregion animated:YES];
    MapPin *annotation = [[MapPin alloc]init];
    annotation.coordinate = center;
    annotation.title=@"Starting Point";
    annotation.subtitle=[NSString stringWithFormat:@"Speed:%@, %@, %@",speed[0],date[0],time_map[0]];
    
    [mapview1 addAnnotation:annotation];
    //NSMutableArray *locations = [[NSMutableArray alloc] init];
    NSMutableArray *coord = [[NSMutableArray alloc] init];
    // str=@"Track history";
    int points =(int) [latitude1 count];
    for (int j=0; j<points; j=j+1) {
        [coord addObject:[NSString stringWithFormat:@"%@, %@",latitude1[j],longitude1[j]]];
        
    }
    
    
    
    
    NSLog(@"coor%@",coord);
    for (int i=1; i< coord.count-1; i++) {
        NSString *str1 = [NSString stringWithFormat:@"Speed:%@, %@, %@$%@",speed[i],date[i],time_map[i],address[i]];
        [self addPinWithTitle:str1 AndCoordinate:coord[i]];
        
        
    }
    //NSLog(@"annotation:%lu",(unsigned long)[annotations count]);
    //NSLog(@"latitude:%lu",(unsigned long)[latitude1 count]);
    /*  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
     [self addressMapping];
     });*/
    
    //[self performSelectorInBackground:@selector(addressMapping) withObject:nil];
    
    /*   dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
     
     dispatch_async(queue,^{
     [self addressMapping];
     
     
     });*/
    
    
    MapPin *annotation1 = [[MapPin alloc]init];
    annotation1.coordinate = dest;
    annotation1.title=@"Finishing Point";
    annotation1.subtitle=[NSString stringWithFormat:@"Speed:%@, %@, %@",speed[size11-1],date[size11-1],time_map[size11-1]];
    
    [mapview1 addAnnotation:annotation1];
    
    
    CLLocationCoordinate2D coordinates[coord.count];
    //NSArray *c =[[NSArray alloc]init];
    //int coordinateIndex = 0;
    
    for(int i=0;i<latitude1.count;i++){
        double x = [latitude1[i] doubleValue];
        double y = [longitude1[i] doubleValue];
        
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = x;
        coordinate.longitude = y;
        
        coordinates[i] = coordinate;
        
    }
    
    MKPolyline *line = [MKPolyline polylineWithCoordinates:coordinates count:coord.count];
    [self.mapview1 addOverlay:line];

    
    /*CGContextRef cxt = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(cxt, [UIColor blackColor].CGColor);
    CGPoint point1 = [self.mapview1 convertCoordinate:coordinates[0] toPointToView:self.mapview1];
    CGPoint point2 = [self.mapview1 convertCoordinate:coordinates[1] toPointToView:self.mapview1];
    TRDrawLineWithArrow(cxt, point1 ,point2, 5, 3);*/
    
}


- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    MKOverlayRenderer *overlayRender = nil;
    MKPolylineRenderer *routeLineRender = [[MKPolylineRenderer alloc] initWithPolyline:overlay];
    routeLineRender.fillColor = [UIColor colorWithRed:0.3f green:0.4f blue:0.0f alpha:0.75f];
    routeLineRender.strokeColor = [UIColor colorWithRed:0.3f green:0.4f blue:0.0f alpha:0.75f];
    routeLineRender.lineWidth = 3;
    overlayRender = routeLineRender;
    return overlayRender;
}
- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay
{
    MKOverlayView *overlayView = nil;
    MKPolylineView *routeLineView = [[MKPolylineView alloc] initWithPolyline:overlay];
    routeLineView.strokeColor = [UIColor colorWithRed:0.3f green:0.4f blue:0.0f alpha:0.75f];    routeLineView.fillColor = [UIColor colorWithRed:0.3f green:0.4f blue:0.0f alpha:0.75f];    routeLineView.lineWidth = 3;
    routeLineView.lineCap=kCGLineCapRound;
    overlayView = routeLineView;
    return overlayView;
}

void TRDrawLineWithArrow(CGContextRef CXT, CGPoint FROMPOINT, CGPoint TOPOINT, CGFloat WIDTH, CGFloat ARROWSIZEMULTIPLE)
{
    CGFloat         rise = TOPOINT.y - FROMPOINT.y;
    CGFloat         run = TOPOINT.x - FROMPOINT.x;
    
    // trig
    CGFloat         length = sqrt(rise*rise + run+run);
    CGFloat         angle = atan2(rise, run);
    
    // the length of our arrowhead
    CGFloat         arrowLen = WIDTH*ARROWSIZEMULTIPLE;
    
    // push graphics context
    CGContextSaveGState(CXT);
    
    // transform context according to line's origin and angle
    CGContextTranslateCTM(CXT, FROMPOINT.x, FROMPOINT.y);
    CGContextRotateCTM(CXT, angle);
    
    // draw straight line
    CGContextMoveToPoint(CXT, 0, -WIDTH/2.);
    CGContextAddLineToPoint(CXT, 0, WIDTH/2.);
    CGContextAddLineToPoint(CXT, length-arrowLen, WIDTH/2.);
    
    // draw arrowhead
    CGContextAddLineToPoint(CXT, length-arrowLen, (WIDTH*ARROWSIZEMULTIPLE)/2.);
    CGContextAddLineToPoint(CXT, length, 0);
    CGContextAddLineToPoint(CXT, length-arrowLen, -(WIDTH*ARROWSIZEMULTIPLE)/2.);
    
    CGContextAddLineToPoint(CXT, length-arrowLen, -WIDTH/2.);
    CGContextAddLineToPoint(CXT, 0, -WIDTH/2.);
    
    // fill the path
    CGContextFillPath(CXT);
    
    // pop graphics context
    CGContextRestoreGState(CXT);
}

-(void)addressMapping{
    NSLog(@"latitude:%lu",(unsigned long)[latitude1 count]);
    
    for(int k =0;k<latitude1.count;k++){
        double lat_in=[latitude1[k] doubleValue];
        double lng_in = [longitude1[k] doubleValue];
        
        NSString *urlString = [NSString stringWithFormat:@"http://45.33.22.249/reverse_geocode.php?lat=%f&lng=%f",lat_in,lng_in];
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:urlString] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            //  NSLog(@"Retrieved Data:%@,%@",data,response);
            NSLog(@"---------------------------------------");
            NSLog(@"error:%@",error);
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            //if (error == nil) {
            
            //NSString *text = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"%@",json);
            
            
        }];
        
        [dataTask resume];
        
        
        MKPointAnnotation *test = annotations[k];
        test.subtitle=@"testing";
        [self.mapview1 addAnnotation:test];
    }
    
    
}

-(void)addPinWithTitle:(NSString *)title AndCoordinate:(NSString *)strCoordinate
{
    MKPointAnnotation *mapPin = [[MKPointAnnotation alloc] init];
    
    // clear out any white space
    strCoordinate = [strCoordinate stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    // convert string into actual latitude and longitude values
    NSArray *components = [strCoordinate componentsSeparatedByString:@","];
    
    double latitude2 = [components[0] doubleValue];
    double longitude2 = [components[1] doubleValue];
    
    // setup the map pin with all data and add to map view
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude2, longitude2);
    NSArray *titles=[title componentsSeparatedByString:@"$"];
    //[annotations addObject:mapPin];
    mapPin.title = titles[0];
    //mapPin.subtitle=titles[1];
    mapPin.coordinate = coordinate;
    
    [self.mapview1 addAnnotation:mapPin];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    
    MKAnnotationView *start = [[MKAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:@"Custom Pins"];
    
    if ([annotation.title isEqualToString:@"Starting Point"]) {
        //start.pinColor = MKPinAnnotationColorGreen;
        start.image = [UIImage imageNamed:@"pin-red.png"];
        //start.animatesDrop = YES;
        //start.image="/Users/Raghee/Downloads/images1/factor-icon-geofencing1-300x300.png";
    }
    else if ([annotation.title isEqualToString:@"Finishing Point"]){
        //start.pinColor = MKPinAnnotationColorPurple;
        //start.animatesDrop = YES;
        start.image =[UIImage imageNamed:@"Flag.png"];
        
    }
    else{
        start.image = [UIImage imageNamed:@"pin-black.png"];
    }
    start.canShowCallout = YES;
    [start setSelected:YES animated:YES];
    return start;
    
    
}

/*- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay{
 if([overlay isKindOfClass:[MKPolyline class]])
 {
 
 MKPolylineView *lineview = [[MKPolylineView alloc] initWithOverlay:overlay];
 lineview.strokeColor = [[UIColor blueColor]colorWithAlphaComponent:0.5];
 lineview.lineWidth = 2.0;
 return lineview;
 
 
 
 }
 else{
 
 return  0;
 }
 
 
 
 
 
 }
 */

/*- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay{
    
    
    if ([overlay isKindOfClass:[MKPolyline class]]) {
        MKPolylineRenderer *pr = [[MKPolylineRenderer alloc] initWithPolyline:overlay];
        pr.strokeColor = [UIColor blueColor];
        pr.lineWidth = 2.0;
        return pr;
    }
    else{
        
        return 0;
    }
    
    
}*/

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    
    MapPin* annotation=view.annotation;
    
    NSLog(@"tapped:%f\n",annotation.coordinate.latitude);
    
    NSData *objectData;
    //NSString *url= @"http://45.33.22.249/reverse_geocode.php";
    NSString *url  = @"http://nominatim-load-1807280891.us-east-1.elb.amazonaws.com/reverse_geocode_tracking.php";
    NSString *urlstr=[NSString stringWithFormat:@"%@?lat=%f&lng=%f",url,annotation.coordinate.latitude,annotation.coordinate.longitude];

    NSString *locationString=[NSString stringWithContentsOfURL :[NSURL URLWithString:urlstr] encoding:NSASCIIStringEncoding error:nil];
    objectData=[locationString dataUsingEncoding:NSUTF8StringEncoding];
    NSArray *recData =[NSJSONSerialization JSONObjectWithData:objectData  options:0 error:nil];
    NSLog(@"LocationString:%@\n  Recievedata=%@\n",locationString,recData);
    
    if (recData != nil && recData.count >0) {
        NSDictionary *element= [recData firstObject];
        NSLog(@"ELement:%@",element);
        NSArray *address=element[@"Address"];
        if ([address count]!=0) {
            if (![address[0] isEqualToString:@""]) {
                annotation.subtitle=address[0];
            }
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)changeMapView:(id)sender {
    
    switch (_mapcontrol.selectedSegmentIndex) {
        case 0:
            mapview1.mapType=MKMapTypeStandard;
            
            break;
        case 1:
            mapview1.mapType=MKMapTypeSatellite;
            break;
        case 2:
            mapview1.mapType=MKMapTypeHybrid;
            break;
            
            
        default:
            break;
    }
    
}
@end
