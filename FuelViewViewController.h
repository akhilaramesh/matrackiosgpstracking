//
//  FuelViewViewController.h
//  HotRodTV0
//
//  Created by Raghee Chandran M on 2/12/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FuelViewViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *fuelView;

@end
