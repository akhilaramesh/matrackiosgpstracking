//
//  initialScreenViewController.m
//  Matrack
//
//  Created by Raghee Chandran M on 7/21/16.
//  Copyright © 2016 matrack. All rights reserved.
//

#import "initialScreenViewController.h"
NSString *common_path1=@"";
//NSString *path_1=@"";
NSMutableArray *url;
int arrayIndex=0;
float timer=3.0;
@interface initialScreenViewController ()

@end

@implementation initialScreenViewController
@synthesize iconLoader;


- (void)viewDidLoad {
    [super viewDidLoad];
    [self bounceImage:iconLoader];
    // progress.progress=0.0;
    common_path1 = @"";
//    path_1=@"";
//    NSLog(@"server:%@,%@",common_path1,path_1);
    url = [[NSMutableArray alloc] init];
    
    [url setObject:@"http://107.170.196.31/" atIndexedSubscript:0];
    [url setObject:@"http://45.79.3.62/" atIndexedSubscript:1];
    //_Logoff.hidden = YES;[common_path isEqual:@""]
    //[self performSelectorOnMainThread:@selector(getServerStatus) withObject:nil waitUntilDone:NO];
    [self getServerStatus];
    //[SimplePingHelper ping:url[0] target:self sel:@selector(pingResult:)];
}
-(void)bounceImage:(UIImageView *)imageView{
    CGPoint origin=imageView.center;
    CGPoint target = CGPointMake(imageView.center.x, imageView.center.y-20);
    CABasicAnimation *bounce = [CABasicAnimation animationWithKeyPath:@"position.y"];
    bounce.duration=1.0;
    bounce.fromValue=[NSNumber numberWithInt:origin.y];
    bounce.toValue=[NSNumber numberWithInt:target.y];
    bounce.repeatCount=HUGE_VALF;
    bounce.autoreverses=YES;
    [imageView.layer addAnimation:bounce forKey:@"position"];
    /*CABasicAnimation *rotation;
    rotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    rotation.fromValue = [NSNumber numberWithFloat:0];
    rotation.toValue = [NSNumber numberWithFloat:(2 * M_PI)];
    rotation.duration = 5.0f; // Speed
    rotation.repeatCount = HUGE_VALF; // Repeat forever. Can be a finite number.
    [imageView.layer removeAllAnimations];
    [imageView.layer addAnimation:rotation forKey:@"Spin"];
     */
    
    
}
-(void) getServerStatus{
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    config.timeoutIntervalForResource=timer;
    config.timeoutIntervalForRequest=timer;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    NSLog(@"url:%@",url[arrayIndex]);
    NSString *urlString = url[arrayIndex];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:urlString] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            if ([url count] == arrayIndex+1) {
                arrayIndex=0;
                timer+=1.0;
            }else{
                arrayIndex++;
            }
            NSString *val= [error.userInfo valueForKey:NSLocalizedDescriptionKey];
            if([val isEqualToString:@"The Internet connection appears to be offline."]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"The Internet connection appears to be offline." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                     if([common_path1 isEqual:@""]){
                         common_path1=url[0];
                         arrayIndex=0;
                        UIStoryboard *storyBoard = self.storyboard;
                        UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"loginscreen"];
                        [self presentViewController:vc animated:YES completion:nil];
                     }
                });
            }else
                [self getServerStatus];
        }else{
            if([common_path1 isEqual:@""]){
                  common_path1=url[arrayIndex];
                arrayIndex=0;
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[showActivity stopAnimating];
                    //showActivity.hidden=YES;
                    UIStoryboard *storyBoard = self.storyboard;
                    UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"loginscreen"];
                    [self presentViewController:vc animated:YES completion:nil];
                });
            }
            
        }
        
    }];
    
    
    [dataTask resume];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
