//
//  FirstViewController.h
//  HotRodTV0
//
//  Created by Raghee Chandran M on 1/28/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "initialScreenViewController.h"
extern NSString *master_flag;
extern NSString *track_flag;
extern NSString *report_flag;
extern NSString *change_flag;extern NSString *path;
extern NSString *usr;
extern NSString *common_path;
extern NSMutableArray *driver_name;
extern NSMutableArray *latitude;
extern NSMutableArray *longitude;
@interface FirstViewController :UIViewController <UIWebViewDelegate,UITextFieldDelegate>
{
    IBOutlet UIScrollView *bgScrollView;
    NSMutableData *receivedData;
    NSString *filePath;
    NSString *str;
    BOOL connectionSuccess;
}
//@property (strong, nonatomic) IBOutlet UIButton *Menu;
@property (strong, nonatomic) NSArray *myArray1;
@property (weak, nonatomic) IBOutlet UITextField *user;
@property (weak, nonatomic) IBOutlet UILabel *failed_msg;

@property (weak, nonatomic) IBOutlet UITextField *pass;
- (IBAction)SignIn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;


@end
