//
//  FifthViewController.h
//  HotRodTV0
//
//  Created by Raghee Chandran M on 1/29/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//
#import <UIKit/UIKit.h>
extern NSString *option;
extern NSArray *driverArray;
extern NSMutableArray *driverlist1;
@interface FifthViewController : UIViewController <UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate>{
    NSMutableData *receivedData;
    
    NSString *filePath;
    NSString *str;
    //NSArray *myArray;
    
    BOOL connectionSuccess;
    int size1;   // UIDatePicker *datepicker;
    //UIDatePicker *datepicker1;
    NSInteger textfieldTag;
}

@property (weak, nonatomic) IBOutlet UIPickerView *CatPicker;
@property (weak, nonatomic) IBOutlet UITextField *category;
- (IBAction)Done:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *CatPickContainer;
@property (strong, nonatomic) NSString *s;
@property (strong, nonatomic) NSArray *myArray;
@property (strong, nonatomic) NSMutableArray *myArray1;
@property (strong, nonatomic) NSArray *content;
@end
