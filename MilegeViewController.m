//
//  MilegeViewController.m
//  HotRodTV0
//
//  Created by Raghee Chandran M on 1/29/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import "MilegeViewController.h"
#import "FirstViewController.h"

@interface MilegeViewController ()
{
    NSDate *startSeletedDate;
}
@end
NSString *start_mileage=@"";
NSString *end_mileage=@"";
NSDate *now;
NSString *s1=@"";
NSMutableArray *driverlist3;
int ixm=0;
int iym=0;
@implementation MilegeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    now = [NSDate date];
    NSDateFormatter *nowformatter = [[NSDateFormatter alloc]init];
    [nowformatter setDateFormat:@"MM/dd/YYYY"];
    NSString *now1=[nowformatter stringFromDate:now];
    _StartDate_Mileage.text=[NSString stringWithFormat:@"%@",now1];
    start_mileage = [NSString stringWithFormat:@"%@",now1];
    end_mileage= [NSString stringWithFormat:@"%@",now1];
    _EndDate_Mileage.text=[NSString stringWithFormat:@"%@",now1];
    _StartDate_Mileage.delegate = self;
    datepicker_Mileage=[[UIDatePicker alloc] init];
    [datepicker_Mileage setMaximumDate:now];
    datepicker_Mileage.datePickerMode=UIDatePickerModeDate;
    [datepicker_Mileage addTarget:self action:@selector(ShowSelectedDate) forControlEvents:UIControlEventValueChanged];
    _StartDate_Mileage.inputView = datepicker_Mileage;
    // _Enddate.inputView = datepicker;
    
    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,230,44)];
    [toolBar setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donePressed)];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space, doneBtn,nil]];
    [self.StartDate_Mileage setInputAccessoryView:toolBar];
    
    
    
    _EndDate_Mileage.delegate = self;
    datepicker1_Mileage=[[UIDatePicker alloc] init];
    [datepicker1_Mileage setMinimumDate:datepicker_Mileage.date];
    [datepicker1_Mileage setMaximumDate:now];
    datepicker1_Mileage.datePickerMode=UIDatePickerModeDate;
    [datepicker1_Mileage addTarget:self action:@selector(ShowSelectedDate1) forControlEvents:UIControlEventValueChanged];
    _EndDate_Mileage.inputView = datepicker1_Mileage;
    // _Enddate.inputView = datepicker;
    
    UIToolbar *toolBar1=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,230,44)];
    [toolBar1 setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn1=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donePressed1)];
    UIBarButtonItem *space1=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar1 setItems:[NSArray arrayWithObjects:space1, doneBtn1,nil]];
    [self.EndDate_Mileage setInputAccessoryView:toolBar1];
    NSString *urlString = [NSString stringWithFormat:@"%@/driver_display.php",path];
    NSURLRequest *theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:(60.0)];
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    if (theConnection) {
        receivedData = [NSMutableData data];
        NSString *receivedDataString = [[NSString alloc]initWithData:receivedData encoding:NSUTF8StringEncoding];
        //NSLog(@"This1: %@",receivedData);
        
        NSLog(@"This2: %@",receivedDataString);
        
    }else
    {
        
        //NSLog(@"Connection Failed");
    }
    
    
    
}



- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [receivedData setLength:0];
   // NSLog(@"This3 :%@",receivedData);
    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    [receivedData appendData:data];
    filePath = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    
    connectionSuccess = YES;
    
    //NSLog(@"This4 :%@",[filePath stringByReplacingOccurrencesOfString:@"-" withString:@""]);
    //NSLog(@"This5 ---->test--->:%@",receivedData);
    //NSLog(@"%@",filePath);
    filePath = [filePath stringByReplacingOccurrencesOfString:@"[" withString:@""];
    filePath = [filePath stringByReplacingOccurrencesOfString:@"]" withString:@""];
   // NSLog(@"%@",filePath);
    self.myArray = [filePath componentsSeparatedByString:@","];
    // int sizea = [myArray count];
    //[self getData];
    driverlist3 = [[NSMutableArray alloc]init];
    for(s1 in _myArray)
    {
        
        s1 = [s1 stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        [driverlist3 addObject:s1];
        
    }
    //driverlist = [[NSMutableArray alloc]initWithArray:myArray];
   // NSLog(@"driverarray------------>%@",driverlist3);
    
}

- (void)ShowSelectedDate
{
    [self startDateSelected:false];
}

-(void)startDateSelected:(BOOL)done{
    NSString *str = [[CommanDataHandler sharedInstance] startDateSelected:datepicker_Mileage.date endDate:datepicker1_Mileage.date isdone:done];
    if (![str isEqualToString:@""]) {
        [datepicker1_Mileage setMinimumDate:datepicker_Mileage.date];
        self.StartDate_Mileage.text=str;
        start_mileage = str;
    }else{
        datepicker_Mileage.date = [CommanDataHandler getDateFromString:self.StartDate_Mileage.text];
    }
//    NSDateFormatter* formatter_Track =[[NSDateFormatter alloc]init];
//    [formatter_Track setDateFormat:@"MM/dd/yyyy"];
//    NSString *srt = [formatter_Track stringFromDate:datepicker_Mileage.date];
//    NSString *nowstr = [formatter_Track stringFromDate:now];
//    NSComparisonResult result = [nowstr compare:srt];
//    if (result == -1) {
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Attention!" message:@"Invalid Date- Please select date less than current date!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//        [alert show];
//    }
//    else{
//        self.StartDate_Mileage.text=srt;
//        start_mileage = str;
//    }
}

-(void)endDateSelected:(BOOL)done{
    NSString*endDate= [[CommanDataHandler sharedInstance] endDateSelected:datepicker_Mileage.date endDate:datepicker1_Mileage.date isDone:done];
    if (![endDate isEqualToString:@""]) {
        [datepicker_Mileage setMaximumDate:datepicker1_Mileage.date];
        self.EndDate_Mileage.text=endDate;
        end_mileage =endDate;
    }
    else{
        datepicker1_Mileage.date = [CommanDataHandler getDateFromString:self.EndDate_Mileage.text];
    }
//    NSDateFormatter *formatter1_Track =[[NSDateFormatter alloc]init];
//    [formatter1_Track setDateFormat:@"MM/dd/yyyy"];
//    NSString *st1 = [formatter1_Track stringFromDate:datepicker_Mileage.date];
//    NSString *en = [formatter1_Track stringFromDate:datepicker1_Mileage.date];
//    NSString *nowstr = [formatter1_Track stringFromDate:now];
//    NSComparisonResult result = [nowstr compare:en];
//    NSComparisonResult res = [en compare:st1];
//    BOOL val=true;
//    if (result == -1) {
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Attention!" message:@"Invalid Date-Please select another date!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//        [alert show];
//        val= false;
//    }
//    if (res == -1) {
//        UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Attention!" message:@"Invalid Dates-End date should be larger than Start Date" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//        [alert1 show];
//        val= false;
//    }
//    if(val){
//        self.EndDate_Mileage.text=en;
//        end_mileage =en;
//    }
}
- (void)donePressed{
    [self startDateSelected:true];
    [self.view endEditing:true];
}

- (void)ShowSelectedDate1
{
    [self endDateSelected:false];
}
- (void)donePressed1{
    [self endDateSelected:true];
    [self.view endEditing:true];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
