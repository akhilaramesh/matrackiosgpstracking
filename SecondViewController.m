//
//  SecondViewController.m
//  HotRodTV0
//
//  Created by Raghee Chandran M on 1/29/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import "SecondViewController.h"
#import "FirstViewController.h"
#import "CommanDataHandler.h"

@interface SecondViewController ()
{
    NSDate *now;
}
@end
NSString *start_Track=@"";
NSString *end_Track=@"";
@implementation SecondViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    now = [NSDate date];
    NSDateFormatter *nowformatter = [[NSDateFormatter alloc]init];
    [nowformatter setDateFormat:@"MM/dd/YYYY"];
    NSString *now1=[nowformatter stringFromDate:now];
    _StartDate_Track.text=[NSString stringWithFormat:@"%@",now1];
    start_Track = [NSString stringWithFormat:@"%@",now1];
    end_Track = [NSString stringWithFormat:@"%@",now1];
    _EndDate_Track.text=[NSString stringWithFormat:@"%@",now1];
    _StartDate_Track.delegate = self;
    datepicker_Track=[[UIDatePicker alloc] init];
    [datepicker_Track setMaximumDate:now];
    datepicker_Track.datePickerMode=UIDatePickerModeDate;
    //[datepicker_Track setDate:[NSDate date]];
    datepicker_Track.date = now;
    [datepicker_Track addTarget:self action:@selector(ShowSelectedDate) forControlEvents:UIControlEventValueChanged];
    
    //_StartDate_Track.inputView = datepicker_Track;
    [_StartDate_Track setInputView:datepicker_Track];
    // [datepicker_Track selectRow:0 ];
    // _Enddate.inputView = datepicker;
    
    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,230,44)];
    [toolBar setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donePressed)];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space, doneBtn,nil]];
    [self.StartDate_Track setInputAccessoryView:toolBar];
    
    
    
    _EndDate_Track.delegate = self;
    datepicker1_Track=[[UIDatePicker alloc] init];
    [datepicker1_Track setMinimumDate:datepicker_Track.date];
    [datepicker1_Track setMaximumDate:now];
    datepicker1_Track.datePickerMode=UIDatePickerModeDate;
    [datepicker1_Track addTarget:self action:@selector(ShowSelectedDate1) forControlEvents:UIControlEventValueChanged];
    _EndDate_Track.inputView = datepicker1_Track;
    // _Enddate.inputView = datepicker;
    
    UIToolbar *toolBar1=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,230,44)];
    [toolBar1 setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn1=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donePressed1)];
    UIBarButtonItem *space1=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar1 setItems:[NSArray arrayWithObjects:space1, doneBtn1,nil]];
    [self.EndDate_Track setInputAccessoryView:toolBar1];
    // Do any additional setup after loading the view.
}
- (void)ShowSelectedDate
{
    [self startDateSelected:false];
}

- (void)donePressed{
    [self startDateSelected:true];
    [self.view endEditing:true];
}
-(void)startDateSelected:(BOOL)done{
    NSString *str = [[CommanDataHandler sharedInstance] startDateSelected:datepicker_Track.date endDate:datepicker1_Track.date isdone:done];
    if (![str isEqualToString:@""]) {
        [datepicker1_Track setMinimumDate:datepicker_Track.date];
        self.StartDate_Track.text=str;
        start_Track = str;
    }else{
        datepicker_Track.date =[CommanDataHandler getDateFromString:self.StartDate_Track.text];
    }
    
}

- (void)ShowSelectedDate1
{
    [self endDateSelected:false];
}

- (void)donePressed1{
    [self endDateSelected:true];
    [self.view endEditing:true];
}
-(void)endDateSelected:(BOOL)done{
    NSString* str= [[CommanDataHandler sharedInstance] endDateSelected:datepicker_Track.date endDate:datepicker1_Track.date  isDone:done];
    if (![str isEqualToString:@""]) {
        [datepicker_Track setMaximumDate:datepicker1_Track.date];
        self.EndDate_Track.text =str;
        end_Track =str;
    }else{
        datepicker_Track.date =[CommanDataHandler getDateFromString:self.EndDate_Track.text];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
