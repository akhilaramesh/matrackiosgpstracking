//
//  MilegeViewController.h
//  HotRodTV0
//
//  Created by Raghee Chandran M on 1/29/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommanDataHandler.h"
extern NSString *start_mileage;
extern NSString *end_mileage;
extern NSMutableArray *driverlist3;
@interface MilegeViewController : UIViewController <UITextFieldDelegate>
{
    
    UIDatePicker *datepicker_Mileage;
    UIDatePicker *datepicker1_Mileage;
    NSDateFormatter *formatter_Mileage;
    NSDateFormatter *formatter1_Mileage;
   
  
        NSMutableData *receivedData;
        
        NSString *filePath;
        NSString *str;
        //NSArray *myArray;
        
        BOOL connectionSuccess;
        int size1;
    
    
   
}

@property (weak, nonatomic) IBOutlet UITextField *StartDate_Mileage;
//@property (strong, nonatomic) NSString *s;
@property (strong, nonatomic) NSArray *myArray;
@property (weak, nonatomic) IBOutlet UITextField *EndDate_Mileage;
@end
