//
//  ThirdViewController.m
//  HotRodTV0
//
//  Created by Raghee Chandran M on 1/29/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import "ThirdViewController.h"
#import "FirstViewController.h"

@interface ThirdViewController ()

@end
NSString *s3=@"";
NSMutableArray *driverlist_cmn;
@implementation ThirdViewController
@synthesize myArray;
@synthesize travellogBtn;
@synthesize fuelBtn;
@synthesize dtcBtn;
@synthesize driverSafetyButn;
@synthesize mileageBtn;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    travellogBtn.titleLabel.minimumScaleFactor=10./ travellogBtn.titleLabel.font.pointSize;
    travellogBtn.titleLabel.adjustsFontSizeToFitWidth=YES;
    fuelBtn.titleLabel.minimumScaleFactor=8./fuelBtn.titleLabel.font.pointSize;
    fuelBtn.titleLabel.adjustsFontSizeToFitWidth=YES;
    dtcBtn.titleLabel.minimumScaleFactor=8./dtcBtn.titleLabel.font.pointSize;
    dtcBtn.titleLabel.adjustsFontSizeToFitWidth=YES;
    driverSafetyButn.titleLabel.minimumScaleFactor=8./driverSafetyButn.titleLabel.font.pointSize;
   driverSafetyButn.titleLabel.adjustsFontSizeToFitWidth=YES;
    mileageBtn.titleLabel.minimumScaleFactor=8./mileageBtn.titleLabel.font.pointSize;
    mileageBtn.titleLabel.adjustsFontSizeToFitWidth=YES;
    NSString *urlString = [NSString stringWithFormat:@"%@/driver_display.php",path];
    NSURLRequest *theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:(60.0)];
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    if (theConnection) {
        receivedData = [NSMutableData data];
        NSString *receivedDataString = [[NSString alloc]initWithData:receivedData encoding:NSUTF8StringEncoding];
        //NSLog(@"This1: %@",receivedData);
        
        NSLog(@"This2: %@",receivedDataString);
        
    }else
    {
        
        //NSLog(@"Connection Failed");
    }
    
    
    
    
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    [receivedData setLength:0];
    // NSLog(@"This3 :%@",receivedData);
    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    [receivedData appendData:data];
    filePath = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    
    connectionSuccess = YES;
    
    // NSLog(@"This4 :%@",[filePath stringByReplacingOccurrencesOfString:@"-" withString:@""]);
    //NSLog(@"This5 ---->test--->:%@",receivedData);
    //NSLog(@"%@",filePath);
    filePath = [filePath stringByReplacingOccurrencesOfString:@"[" withString:@""];
    filePath = [filePath stringByReplacingOccurrencesOfString:@"]" withString:@""];
    //NSLog(@"%@",filePath);
    self.myArray = [filePath componentsSeparatedByString:@","];
    // int sizea = [myArray count];
    //[self getData];
    driverlist_cmn = [[NSMutableArray alloc]init];
    for(s3 in myArray)
    {
        
        s3 = [s3 stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        [driverlist_cmn addObject:s3];
        
    }
    //driverlist = [[NSMutableArray alloc]initWithArray:myArray];
    // NSLog(@"driverarray------------>%@",driverlist_cmn);
    
}


// Do any additional setup after loading the view.


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
