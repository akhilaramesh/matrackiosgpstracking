//
//  BaseViewController.m
//  MaTrack
//
//  Created by Kushal choudhari on 10/01/18.
//  Copyright © 2018 Sieva Networks. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()
{
    UIScrollView *bgScrollView;
}
@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}
//for no
-(void)addNotification:(UIScrollView*)bgScroll{
    bgScrollView =bgScroll;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

-(void)keyboardWillShow:(NSNotification *)notification {
    [self adjustingHeight:YES notification:notification];
}

-(void) keyboardWillHide:(NSNotification *) notification {
    [self adjustingHeight:false notification:notification];
}

-(void) adjustingHeight:(BOOL )show notification:(NSNotification *)notification{
    NSDictionary *userInfo = notification.userInfo;
    //condition for only above iphone8.
    NSString *key = [[UIScreen mainScreen] bounds].size.height >= 812.0f ? UIKeyboardFrameBeginUserInfoKey:UIKeyboardFrameEndUserInfoKey;
    
    NSValue* keyboardFrameBegin = [userInfo valueForKey:key];
    CGRect keyboardFrame = [keyboardFrameBegin CGRectValue];
    float  changeInHeight = (keyboardFrame.size.height+40)*(show ?1:0);
    if (changeInHeight== 0)
        changeInHeight = 150.0f;
    
    UIEdgeInsets edge =  UIEdgeInsetsMake(0, 0, changeInHeight, 0);
    [bgScrollView setContentInset:edge];
    [bgScrollView setScrollIndicatorInsets:edge];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
