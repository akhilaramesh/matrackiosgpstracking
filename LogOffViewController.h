//
//  LogOffViewController.h
//  HotRodTV0
//
//  Created by Raghee Chandran M on 2/2/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FirstViewController.h"


@interface LogOffViewController : UIViewController <UIWebViewDelegate,UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet UIButton *retryButton;
@property (weak, nonatomic) IBOutlet UIWebView *Logoff;
@end
