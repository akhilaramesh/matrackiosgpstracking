//
//  FuelViewController.m
//  HotRodTV0
//
//  Created by Raghee Chandran M on 1/29/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import "FuelViewController.h"
#import "FirstViewController.h"
#import "CommanDataHandler.h"
@interface FuelViewController ()

@end
NSString *start_fuel=@"";
NSString *end_fuel=@"";
int ixf=0;
int iyf=0;
NSDate *now;
@implementation FuelViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    now = [NSDate date];
    NSDateFormatter *nowformatter = [[NSDateFormatter alloc]init];
    [nowformatter setDateFormat:@"MM/dd/YYYY"];
    NSString *now1=[nowformatter stringFromDate:now];
    _StartDate_Fuel.text=[NSString stringWithFormat:@"%@",now1];
    start_fuel = [NSString stringWithFormat:@"%@",now1];
    end_fuel= [NSString stringWithFormat:@"%@",now1];
    _EndDate_Fuel.text=[NSString stringWithFormat:@"%@",now1];
    _StartDate_Fuel.delegate = self;
    datepicker_Fuel=[[UIDatePicker alloc] init];
    [datepicker_Fuel setMaximumDate:now];
    datepicker_Fuel.datePickerMode=UIDatePickerModeDate;
    [datepicker_Fuel addTarget:self action:@selector(ShowSelectedDate) forControlEvents:UIControlEventValueChanged];
    _StartDate_Fuel.inputView = datepicker_Fuel;
    // _Enddate.inputView = datepicker;
    
    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,230,44)];
    [toolBar setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donePressed)];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space, doneBtn,nil]];
    [self.StartDate_Fuel setInputAccessoryView:toolBar];
    
    
    _EndDate_Fuel.delegate = self;
    datepicker1_Fuel=[[UIDatePicker alloc] init];
    [datepicker1_Fuel setMinimumDate:datepicker_Fuel.date];
    [datepicker1_Fuel setMaximumDate:now];
    datepicker1_Fuel.datePickerMode=UIDatePickerModeDate;
    [datepicker1_Fuel addTarget:self action:@selector(ShowSelectedDate1) forControlEvents:UIControlEventValueChanged];
    _EndDate_Fuel.inputView = datepicker1_Fuel;
    
    UIToolbar *toolBar1=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,230,44)];
    [toolBar1 setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn1=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donePressed1)];
    UIBarButtonItem *space1=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar1 setItems:[NSArray arrayWithObjects:space1, doneBtn1,nil]];
    [self.EndDate_Fuel setInputAccessoryView:toolBar1];
    // Do any additional setup after loading the view.
}
- (void)ShowSelectedDate
{
    [self startDateSelected:false];
}

- (void)donePressed{
    [self startDateSelected:true];
    [self.view endEditing:true];
}

-(void)startDateSelected:(BOOL)done{
    NSString *str = [[CommanDataHandler sharedInstance] startDateSelected:datepicker_Fuel.date endDate:datepicker1_Fuel.date isdone:done];
    if (![str isEqualToString:@""]) {
        [datepicker1_Fuel setMinimumDate:datepicker_Fuel.date];
        self.StartDate_Fuel.text=str;
        start_fuel = str;
    }else{
         datepicker_Fuel.date = [CommanDataHandler getDateFromString:self.StartDate_Fuel.text];
    }
}

- (void)ShowSelectedDate1
{
    [self endDateSelected:false];
}
- (void)donePressed1{
    [self endDateSelected:true];
    [self.view endEditing:true];
    
}

-(void)endDateSelected:(BOOL)done{
    NSString* str= [[CommanDataHandler sharedInstance] endDateSelected:datepicker_Fuel.date endDate:datepicker1_Fuel.date isDone:done];
    if (![str isEqualToString:@""]) {
        [datepicker_Fuel setMaximumDate:datepicker1_Fuel.date];
        self.EndDate_Fuel.text =str;
        end_fuel =str;
    }else{
        datepicker1_Fuel.date = [CommanDataHandler getDateFromString:self.EndDate_Fuel.text];
    }
 
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
