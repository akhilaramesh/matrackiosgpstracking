//
//  FuelDriverViewController.h
//  HotRodTV0
//
//  Created by Raghee Chandran M on 2/12/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//


#import <UIKit/UIKit.h>
extern NSString *driver_fuel;
@interface FuelDriverViewController : UIViewController  <UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate>
{
    
    NSMutableData *receivedData;
    
    NSString *filePath;
    NSString *str;
    //NSArray *myArray;
    
    BOOL connectionSuccess;
    int size1;
    
}
@property (weak, nonatomic) IBOutlet UIView *FuelContainer;

@property (weak, nonatomic) IBOutlet UIPickerView *picker_Fuel;

- (IBAction)Done_Fuel:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *Dname_Fuel;
@property (strong, nonatomic) NSArray *myArray;
@property (strong, nonatomic) NSMutableArray *myArray1;
@end
