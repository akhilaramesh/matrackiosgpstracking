//
//  GeofenceViewController.m
//  HotRodTV0
//
//  Created by Raghee Chandran M on 1/29/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import "GeofenceViewController.h"
#import "FirstViewController.h"
#import "FifthViewController.h"
#import "TrackDataViewController.h"
#import "ThirdViewController.h"

@interface GeofenceViewController ()

@end
NSString *driver_safe=@"";
int dr2=0;
NSMutableArray *driverlist4;
NSString *s2;
@implementation GeofenceViewController
@synthesize myArray;
@synthesize picker_safety;
@synthesize myArray1;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //    _picker = [[UIPickerView alloc] init];
   _SafetyContainer.hidden = YES;
    _DriverSafe.delegate = self;
    //NSLog(@"array%@",driverlist_cmn);
    myArray1 = [[NSMutableArray alloc]initWithArray:driver_name];
    _DriverSafe.text=[NSString stringWithFormat:@"%@",myArray1[0]];
    driver_safe=[NSString stringWithFormat:@"%@",myArray1[0]];
    NSString *last=@"none";
    [myArray1 addObject:last];
   // NSLog(@"Array:%@",myArray1);
    size1=(int)[myArray1 count];
   // NSLog(@"%d",size1);   // _content_Track = [[NSArray alloc] initWithObjects:@"ALL",@"STOPS ONLY",@"IGNITION ON/OFF",@"MOVEMENTS ONLY",nil];
    // CatPicker.hidden = NO;
    picker_safety.delegate=self;
    picker_safety.dataSource=self;
    [_DriverSafe setInputView:picker_safety];
    [picker_safety reloadAllComponents];
    //self.myArray =[[NSArray alloc]initWithObjects:@"red", nil];
    
    // Do any additional setup after loading the view.
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    _SafetyContainer.hidden = NO;
    picker_safety.hidden = NO;
    return NO;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}


//  returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return size1;
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return [myArray1 objectAtIndex:row];
    
    
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)componen1t{
    NSString *name = @"none";
    if (myArray1 != nil && myArray1.count >0) {
        name =[myArray1 objectAtIndex:row];
    }
    _DriverSafe.text = name;
    [self driverSelected];
//    str = [myArray1 objectAtIndex:row1];
//    if ([str  isEqual: @"none"]) {
//
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Driver not exists-Please select another driver!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
//
//    }
    //_output.text =str;
   // NSLog(@"selected%@",str);
    //NSLog(@"testing2-------->%d",opt);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)DoneSafe:(id)sender {
    [self driverSelected];
    _SafetyContainer.hidden = YES;
//    if (dr2 == 0) {
//        NSString *str2 = [myArray1 objectAtIndex:0];
//        NSString *str3 = [str2 stringByReplacingOccurrencesOfString:@"\"" withString:@""];
//        _DriverSafe.text = str3;
//        driver_safe =str3;
//        _SafetyContainer.hidden =YES;
//
//
//    }else
//    {
//        NSString *str1=[str stringByReplacingOccurrencesOfString:@"\"" withString:@""];
//        _DriverSafe.text=str1;
//        driver_safe= str1;
//        _SafetyContainer.hidden = YES;
//    }
}
- (IBAction)nextAction:(id)sender {
    NSString *name = _DriverSafe.text;
    if([name isEqualToString:@""] || [name isEqualToString:@"none"] ){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Driver not exists-Please select another driver!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else{
        UIStoryboard *storyBoard = self.storyboard;
        UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"SafetyViewController"];
        [self.navigationController pushViewController:vc animated:true];
    }
}
-(void)driverSelected{
    NSString *driverName = _DriverSafe.text;
    if ([driverName  isEqualToString:@"none"]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Driver not exists-Please select another driver!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        _DriverSafe.text=driverName;
        driver_safe = driverName;
    }
}

@end
