//
//  MapPin.h
//  HotRodTV0
//
//  Created by Raghee Chandran M on 3/31/16.
//  Copyright © 2016 Sieva Networks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MapPin : NSObject <MKAnnotation>
{
    CLLocationCoordinate2D coordinate;
    NSString *title;
    NSString *subtitle;
    int type;
    //MKPinAnnotationColor *color;
    NSString *subtitle1;
    
}

@property (nonatomic,assign) CLLocationCoordinate2D coordinate;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSString *subtitle;
@property (nonatomic,assign) int type;

@property (nonatomic,copy) NSString *subtitle1;
//@property (nonatomic,copy)        int type;

@end

