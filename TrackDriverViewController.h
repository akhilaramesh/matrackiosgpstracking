//
//  TrackDriverViewController.h
//  HotRodTV0
//
//  Created by Raghee Chandran M on 2/11/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import <UIKit/UIKit.h>
extern NSString *driver_Track;
@interface TrackDriverViewController : UIViewController <UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate>

{
    NSMutableData *receivedData;
    
    NSString *filePath;
    NSString *str;
    //NSArray *myArray;
    
    BOOL connectionSuccess;
    int size1;
    
    
}
- (IBAction)DoneBtn_Track:(id)sender;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerdri_Track;

@property (weak, nonatomic) IBOutlet UIView *PickerDriContainer_Track;

@property (weak, nonatomic) IBOutlet UITextField *Driver_Track;

@property (strong, nonatomic) NSArray *myArray;
@property (strong, nonatomic) NSMutableArray *myArray1;
@end
