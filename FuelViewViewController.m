//
//  FuelViewViewController.m
//  HotRodTV0
//
//  Created by Raghee Chandran M on 2/12/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import "FuelViewViewController.h"
#import "FirstViewController.h"
#import "FuelViewController.h"
#import "FuelDriverViewController.h"

@interface FuelViewViewController ()

@end

@implementation FuelViewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *s = [driver_fuel stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *urlString = [NSString stringWithFormat:@"%@/fuelreport_b.php?driver=%@&startdate=%@&enddate=%@",path,s,start_fuel,end_fuel];
    //NSString *urlString = @"%@/trackreport_b_new1.php?driver=%@&startdate=%@&enddate=%@&select=%@",path,driver,start,end,option;
    //NSLog(@"%@",urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [_fuelView loadRequest:urlRequest];     // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
