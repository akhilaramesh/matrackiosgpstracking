//
//  currentlocationViewController.m
//  HotRodTV0
//
//  Created by Raghee Chandran M on 3/31/16.
//  Copyright © 2016 Sieva Networks. All rights reserved.
//

#import "currentlocationViewController.h"
#import "FirstViewController.h"
#import "MapPin.h"
#import <CoreLocation/CoreLocation.h>
#import <AddressBook/AddressBook.h>
@interface currentlocationViewController ()

@end
NSMutableArray *latitude;
NSMutableArray *longitude;
NSMutableArray *address_array;
NSMutableArray *time_date;
NSMutableArray *last_read;
NSMutableArray *battery;
NSString *address2;
NSString *battery_value=@"";
NSArray *final_addrs;
int is_track_removed=0;

@implementation currentlocationViewController
CLLocationManager *locationManager;
CLGeocoder *geocoder;
CLPlacemark *placemark;
@synthesize mymap;
@synthesize refresh;
- (void)viewDidLoad {
    [super viewDidLoad];
    if (![master_flag isEqualToString:@"\"\""]) {
        NSMutableArray *tabs = [NSMutableArray arrayWithArray:[self.tabBarController viewControllers]];
        NSLog(@"tabs %@",tabs);
        if (![track_flag isEqualToString:@"yes"]) {
            is_track_removed=1;
            [tabs removeObjectAtIndex:1];
            [self.tabBarController setViewControllers:tabs];
        }
        if (![report_flag isEqualToString:@"yes"]) {
            // NSMutableArray *tabs = [NSMutableArray arrayWithArray:[self.tabBarController viewControllers]];
            if (is_track_removed == 1) {
                
                [tabs removeObjectAtIndex:1];
                
            }
            else{
                
                [tabs removeObjectAtIndex:2];
            }
            [self.tabBarController setViewControllers:tabs];
        }
        
        
    }else{
        NSMutableArray *tabs = [NSMutableArray arrayWithArray:[self.tabBarController viewControllers]];
        [self.tabBarController setViewControllers:tabs];
    }
    locationManager = [[CLLocationManager alloc] init];
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
        
    }
    //[self performSelector:@selector(mapPlotting) withObject:nil];
    geocoder = [[CLGeocoder alloc] init];
    
    mymap.delegate=self;
    [self getData];
}
-(void)getData{
    [_indicator startAnimating];
    _indicator.hidden =  NO;
//    NSString *urlString = [NSString stringWithFormat:@"%@gpstracking/current_location.php",common_path];
    NSString *urlString = [NSString stringWithFormat:@"%@gpstracking/current_location.php",common_path];
    NSURLSession * session=[NSURLSession sharedSession];
    NSURLSessionDataTask * sessionTask= [session dataTaskWithURL:[NSURL URLWithString:urlString] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [_indicator stopAnimating];
            _indicator.hidden =  YES;
            [self currentLocation:data];
        });
        
    }];
    [sessionTask resume];
    
    
    
}
/*-(void)redirect{
 
 UIStoryboard *storyBoard = self.storyboard;
 UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"initial"];
 [self presentViewController:vc animated:YES completion:nil];
 
 
 }*/
/*- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    [receivedData setLength:0];
    NSLog(@"This3 :%@",receivedData);
    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    [receivedData appendData:data];
    
    
    
}*/
-(void)currentLocation:(NSData *)data{
    

    filePath = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    //NSLog(@"%@",filePath);
    NSLog(@"current_location:%@",filePath);
    filePath = [filePath stringByReplacingOccurrencesOfString:@"[" withString:@""];
    filePath = [filePath stringByReplacingOccurrencesOfString:@"]" withString:@""];
    NSLog(@"%@",receivedData);
    _myArray1 = [filePath componentsSeparatedByString:@","];
    int size= (int)[_myArray1 count];
    driver_name = [[NSMutableArray alloc] init];
    latitude = [[NSMutableArray alloc] init];
    longitude = [[NSMutableArray alloc] init];
    last_read = [[NSMutableArray alloc] init];
    address_array = [[NSMutableArray alloc] init];
    battery = [[NSMutableArray alloc] init];
    for(int i=2;i<size;i++){
        NSArray  *points = [[NSArray alloc]init];
        NSString *element = [[NSString alloc]init];
        element = _myArray1[i];
        element  = [element stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        points = [element  componentsSeparatedByString:@"$"];
        NSLog(@"%@",points);
        [driver_name addObject:points[0]];
        [latitude addObject:points[1]];
        [longitude addObject:points[2]];
        [last_read addObject:points[4]];
        [address_array addObject:points[5]];
        [battery addObject:points[6]];
        // points_value[j] = points;
        //NSMutableArray *sam = [[NSMutableArray alloc] initWithArray:points];
        //[points_value addObject:sam];
        //points_value ins];
        
        
    }
    if ([latitude count]!=0 && [longitude count]!=0) {
        [self mapPlotting];
        NSLog(@"battery:%@",battery);
        //[self zoomMapViewToFitAnnotations:self.mymap animated:NO];
    }}
-(void)mapPlotting{
    [mymap removeAnnotations:mymap.annotations];
    // self.mymap.delegate=self;
    double lat1 = [latitude[0] doubleValue];
    double lng1 = [longitude[0] doubleValue];
    CLLocationCoordinate2D center;
    center.latitude = lat1;
    center.longitude = lng1;
    
    MKCoordinateSpan zoom;
    zoom.latitudeDelta=0.1f;
    zoom.longitudeDelta=0.1f;
    
    MKCoordinateRegion myregion;
    myregion.center = center;
    myregion.span = zoom;
    [mymap setRegion:myregion animated:NO];
    //NSMutableArray *locations = [[NSMutableArray alloc] init];
    
    NSArray *name = [[NSArray alloc] initWithArray:driver_name];
    NSMutableArray *coord = [[NSMutableArray alloc] init];
    // address1 = [[NSMutableArray alloc] init];
    // str=@"Track history";
    int points =(int) [latitude count];
    for (int j=0; j<points; j=j+1) {
        [coord addObject:[NSString stringWithFormat:@"%@, %@",latitude[j],longitude[j]]];
        
        
        
    }
    //NSLog(@"address:%@",address1);
    for (int i=0; i< coord.count; i++) {
        
        NSMutableString *str1 = [NSMutableString stringWithFormat:@"%@, %@$%@",name[i],last_read[i],address_array[i]];
        NSLog(@"str1:%@",str1);
        battery_value=battery[i];
        [self addPinWithTitle:str1 AndCoordinate:coord[i]];
        
        
        
        
        
    }
    refresh.enabled=YES;
    [self zoomMapViewToFitAnnotations:self.mymap animated:NO];
    [_indicator stopAnimating];
    _indicator.hidden =  YES;
    
}

-(void)addPinWithTitle:(NSString *)title AndCoordinate:(NSString *)strCoordinate
{
    MKPointAnnotation *mapPin = [[MKPointAnnotation alloc] init];
    
    // clear out any white space
    strCoordinate = [strCoordinate stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    // convert string into actual latitude and longitude values
    NSArray *components = [strCoordinate componentsSeparatedByString:@","];
    
    double latitude2 = [components[0] doubleValue];
    double longitude2 = [components[1] doubleValue];
    NSArray *titles =[title componentsSeparatedByString:@"$"];
    // setup the map pin with all data and add to map view
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude2, longitude2);
    
   /* NSArray *addr = [titles[1] componentsSeparatedByString:@"|"];
    NSString *fulladres = [NSString stringWithFormat:@"%@,%@,%@",addr[0],addr[1],addr[2]];*/
    mapPin.title = titles[0];
    mapPin.subtitle = battery_value;
   // mapPin.subtitle = fulladres;
    mapPin.coordinate = coordinate;
    
    [self.mymap addAnnotation:mapPin];
    // [self Zoomforpins];
    //[self zoomMapViewToFitAnnotations:self.mymap animated:YES];
    
}


-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    NSString *battryLevel = [NSString stringWithFormat:@"%@",annotation.subtitle];
    MKPinAnnotationView *pinview=[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@""];
    
    UIImageView *baterry = [[UIImageView alloc] initWithFrame:(CGRectMake(0, 0, pinview.frame.size.height-150, pinview.frame.size.height-150))];
    baterry.image = [UIImage imageNamed:@"battery_light1.png"];
    
    UILabel *battery_level = [[UILabel alloc] init];
    battery_level.text= battryLevel;
    [battery_level setFont:[battery_level.font fontWithSize:10.0]];
    
    
    UIStackView *stackView = [[UIStackView alloc] initWithFrame:(CGRectMake(0, 0, pinview.frame.size.height,pinview.frame.size.height))];
    stackView.axis = UILayoutConstraintAxisVertical;
    stackView.distribution = UIStackViewDistributionEqualSpacing;
    stackView.alignment = UIStackViewAlignmentCenter;
    stackView.backgroundColor = [UIColor greenColor];
    
    [stackView addArrangedSubview:baterry];
    [stackView addArrangedSubview:battery_level];
    //[stackView addArrangedSubview:baterry];
    pinview.canShowCallout=YES;
    pinview.leftCalloutAccessoryView=stackView;
    //pinview.image=[UIImage imageNamed:@"battery.png"];
    
    return pinview;
}
-(void)Zoomforpins{
    
    
    
    
    MKMapRect zoomRect = MKMapRectNull;
    for (id <MKAnnotation> annotation in mymap.annotations)
    {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 10.0,10.0);
        
        zoomRect = MKMapRectUnion(zoomRect, pointRect);
    }
    [mymap setVisibleMapRect:zoomRect animated:NO];
}

- (void)zoomMapViewToFitAnnotations:(MKMapView *)mapView animated:(BOOL)animated
{
    
    NSLog(@"entered........");
    NSArray *annotations = mapView.annotations;
    int count = (int)[mapView.annotations count];
    NSLog(@"No of points:%d",count);
    if ( count == 0) { return; } //bail if no annotations
    
    //convert NSArray of id <MKAnnotation> into an MKCoordinateRegion that can be used to set the map size
    //can't use NSArray with MKMapPoint because MKMapPoint is not an id
    MKMapPoint points[count]; //C array of MKMapPoint struct
    for( int i=0; i<count; i++) //load points C array by converting coordinates to points
    {
        CLLocationCoordinate2D coordinate = [(id <MKAnnotation>)[annotations objectAtIndex:i] coordinate];
        points[i] = MKMapPointForCoordinate(coordinate);
    }
    //create MKMapRect from array of MKMapPoint
    MKMapRect mapRect = [[MKPolygon polygonWithPoints:points count:count] boundingMapRect];
    //convert MKCoordinateRegion from MKMapRect
    MKCoordinateRegion region = MKCoordinateRegionForMapRect(mapRect);
    
    //add padding so pins aren't scrunched on the edges
    region.span.latitudeDelta  *= ANNOTATION_REGION_PAD_FACTOR;
    region.span.longitudeDelta *= ANNOTATION_REGION_PAD_FACTOR;
    //but padding can't be bigger than the world
    if( region.span.latitudeDelta > MAX_DEGREES_ARC ) { region.span.latitudeDelta  = MAX_DEGREES_ARC; }
    if( region.span.longitudeDelta > MAX_DEGREES_ARC ){ region.span.longitudeDelta = MAX_DEGREES_ARC; }
    
    //and don't zoom in stupid-close on small samples
    if( region.span.latitudeDelta  < MINIMUM_ZOOM_ARC ) { region.span.latitudeDelta  = MINIMUM_ZOOM_ARC; }
    if( region.span.longitudeDelta < MINIMUM_ZOOM_ARC ) { region.span.longitudeDelta = MINIMUM_ZOOM_ARC; }
    //and if there is a sample of 1 we want the max zoom-in instead of max zoom-out
    if( count == 1 )
    {
        region.span.latitudeDelta = MINIMUM_ZOOM_ARC;
        region.span.longitudeDelta = MINIMUM_ZOOM_ARC;
    }
    [mapView setRegion:region animated:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    
    MapPin* annotation=view.annotation;
    
    NSLog(@"tapped:%f\n",annotation.coordinate.latitude);
    if(annotation.coordinate.latitude>0){
        NSData *objectData;
//        NSString *url= @"http://45.33.22.249/reverse_geocode.php";
        NSString *url  = @"http://nominatim-load-1807280891.us-east-1.elb.amazonaws.com/reverse_geocode_tracking.php";
        NSString *urlstr=[NSString stringWithFormat:@"%@?lat=%f&lng=%f",url,annotation.coordinate.latitude,annotation.coordinate.longitude];
        NSString *locationString=[NSString stringWithContentsOfURL :[NSURL URLWithString:urlstr] encoding:NSASCIIStringEncoding error:nil];
        objectData=[locationString dataUsingEncoding:NSUTF8StringEncoding];
        if (objectData != nil) {
            NSArray *recData =[NSJSONSerialization JSONObjectWithData:objectData  options:0 error:nil];
            NSLog(@"LocationString:%@\n  Recievedata=%@\n",locationString,recData);
            if (recData != nil && recData.count >0) {
            NSDictionary *element= [recData firstObject];
            NSLog(@"ELement:%@",element);
            NSArray *address=element[@"Address"];
            if ([address count]!=0) {
                if (![address[0] isEqualToString:@""])
                    annotation.subtitle=address[0];
                }
            }
        }
    }
}


- (IBAction)changeView:(id)sender {
    switch (_mycontrol.selectedSegmentIndex) {
        case 0:
            mymap.mapType=MKMapTypeStandard;
            break;
        case 1:
            mymap.mapType=MKMapTypeSatellite;
            break;
        case 2:
            mymap.mapType=MKMapTypeHybrid;
            break;
            
        default:
            break;
    }
}

- (IBAction)updateAgain:(id)sender {
    refresh.enabled = NO;
    
    [mymap removeAnnotations:mymap.annotations];
    [self viewDidLoad];
}
@end
