//
//  CommanDataHandler.m
//  MaTrack
//
//  Created by Kushal choudhari on 10/01/18.
//  Copyright © 2018 Sieva Networks. All rights reserved.
//

#import "CommanDataHandler.h"

@implementation CommanDataHandler

+ (CommanDataHandler *)sharedInstance
{
    static CommanDataHandler *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[CommanDataHandler alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}
-(BOOL)checkEndDate:(NSDate*)endDate IsGreaterThanOrEqualStartDate:(NSDate *)startDate{
    if (endDate.timeIntervalSince1970 >= startDate.timeIntervalSince1970)
        return true;
    else
        return false;
}

-(NSString*)startDateSelected:(NSDate*)startDate endDate:(NSDate*)end isdone:(BOOL)done{
    NSDate *newNow = [NSDate date];
    NSDateFormatter* formatter_Track =[[NSDateFormatter alloc]init];
    [formatter_Track setDateFormat:@"MM/dd/yyyy"];
    NSString *srt = [formatter_Track stringFromDate:startDate];
    BOOL result = startDate.timeIntervalSince1970 <= newNow.timeIntervalSince1970;
    BOOL res = end.timeIntervalSince1970 >= startDate.timeIntervalSince1970;
    BOOL val=true;
    if (!result) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Attention!" message:@"Invalid Date- Please select date less than current date!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        val= false;
    }
   else if (!res) {
        UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Attention!" message:@"Invalid Dates-End date should be larger than Start Date" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert1 show];
        val= false;
    }
    if (!val)
        srt =@"";
    
    return srt;
}

-(NSString*)endDateSelected:(NSDate*)startDate endDate:(NSDate*)endDate isDone:(BOOL)done{
     NSDate *newNow = [NSDate date];
    NSDateFormatter *formatter1_Track =[[NSDateFormatter alloc]init];
    [formatter1_Track setDateFormat:@"MM/dd/yyyy"];
    NSString *en = [formatter1_Track stringFromDate:endDate];
    BOOL result = endDate.timeIntervalSince1970 <= newNow.timeIntervalSince1970;
    BOOL res = endDate.timeIntervalSince1970 >= startDate.timeIntervalSince1970;
    BOOL val=true;
    if (!result) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Attention!" message:@"Invalid Date-Please select another date!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        val= false;
    }
    else if (!res ) {
        UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Attention!" message:@"Invalid Dates-End date should be larger than Start Date" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert1 show];
        val= false;
    }
    if (!val) {
        en = @"";
//        en =[formatter1_Track stringFromDate:newNow];
    }
    return en;
}
+(NSDate*)getDateFromString:(NSString *)input{
    NSDateFormatter* formatter_Track =[[NSDateFormatter alloc]init];
    [formatter_Track setDateFormat:@"MM/dd/yyyy"];
    NSDate *date= [formatter_Track dateFromString:input];
    if (date == nil) {
        date = [NSDate new];
    }
    return date;
}

@end
