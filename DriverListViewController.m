//
//  DriverListViewController.m
//  HotRodTV0
//
//  Created by Raghee Chandran M on 2/8/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import "DriverListViewController.h"
#import "FirstViewController.h"
#import "FifthViewController.h"


@interface DriverListViewController ()

@end
NSString *driver=@"";
int dr1=0;
@implementation DriverListViewController

@synthesize myArray;
@synthesize picker;
@synthesize myArray1;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    _pickerContainer.hidden = YES;
    
    picker.hidden = YES;
    
    _output.delegate = self;
    // NSLog(@"array%@",driverlist1);
    myArray1 = [[NSMutableArray alloc]initWithArray:driver_name];
    _output.text=[NSString stringWithFormat:@"%@",myArray1[0]];
    driver=[NSString stringWithFormat:@"%@",myArray1[0]];
    NSString *last=@"none";
    [myArray1 addObject:last];
    // NSLog(@"Array:%@",myArray1);
    size1=(int)[myArray1 count];
    //NSLog(@"%d",size1);   // _content_Track = [[NSArray alloc] initWithObjects:@"ALL",@"STOPS ONLY",@"IGNITION ON/OFF",@"MOVEMENTS ONLY",nil];
    // CatPicker.hidden = NO;
    picker.delegate=self;
    picker.dataSource=self;
    [_output setInputView:picker];
    [picker reloadAllComponents];
    
    
    //self.myArray =[[NSArray alloc]initWithObjects:@"red", nil];
    
    // Do any additional setup after loading the view.
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    // _category.inputView=CatPicker;
    
    
    _pickerContainer.hidden = NO;
    
    picker.hidden = NO;
    
    
    
    
    
    
    
    return NO;
    
}




- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
    
    
}


//  returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return size1;
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return [myArray1 objectAtIndex:row];
    
    
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)componen1t{
    NSString *name = @"none";
    if (myArray1 != nil && myArray1.count >0) {
        name =[myArray1 objectAtIndex:row];
    }
    _output.text = name;
    [self driverSelected];
}
-(void)driverSelected{
    NSString *driverName = _output.text;
    if ([driverName  isEqualToString:@"none"]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Driver not exists-Please select another driver!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        _output.text=driverName;
        driver = driverName;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)DoneBtn:(id)sender {
    [self driverSelected];
   _pickerContainer.hidden = YES;
    
//    if (dr1 == 0) {
//        NSString *dr=[myArray1 objectAtIndex:[picker selectedRowInComponent:0]];
//        NSLog(@"driver selected from picker:%@",dr);
//        NSString *str2 = [myArray1 objectAtIndex:0];
//        NSString *str3 = [str2 stringByReplacingOccurrencesOfString:@"\"" withString:@""];
//        _output.text = str3;
//        driver=str3;
//        _pickerContainer.hidden =YES;
//
//
//    }else
//    {
//        NSString *dr=[myArray1 objectAtIndex:[picker selectedRowInComponent:0]];
//        NSLog(@"driver selected from picker:%@",dr);
//        NSString *str1=[str stringByReplacingOccurrencesOfString:@"\"" withString:@""];
//        _output.text=str1;
//        driver = str1;
//        _pickerContainer.hidden = YES;
//    }
    
}
- (IBAction)submitAction:(id)sender {
    NSString *name = _output.text;
    if([name isEqualToString:@""] || [name isEqualToString:@"none"] ){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Driver not exists-Please select another driver!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else{
        UIStoryboard *storyBoard = self.storyboard;
        UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"TravelLogViewController"];
        [self.navigationController pushViewController:vc animated:true];
    }

}




@end
