//
//  DTCViewController.m
//  HotRodTV0
//
//  Created by Raghee Chandran M on 1/29/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import "DTCViewController.h"
#import "FirstViewController.h"

@interface DTCViewController ()

@end
NSString *start_dtc=@"";
NSString *end_dtc=@"";
NSDate *now;
@implementation DTCViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    now = [NSDate date];
    NSDateFormatter *nowformatter = [[NSDateFormatter alloc]init];
    [nowformatter setDateFormat:@"MM/dd/YYYY"];
    NSString *now1=[nowformatter stringFromDate:now];
    _StartDate_DTC.text=[NSString stringWithFormat:@"%@",now1];
    start_dtc = [NSString stringWithFormat:@"%@",now1];
    end_dtc= [NSString stringWithFormat:@"%@",now1];
    _EndDate_DTC.text=[NSString stringWithFormat:@"%@",now1];
    _StartDate_DTC.delegate = self;
    datepicker_DTC=[[UIDatePicker alloc] init];
    [datepicker_DTC setMaximumDate:now];
    datepicker_DTC.datePickerMode=UIDatePickerModeDate;
    [datepicker_DTC addTarget:self action:@selector(ShowSelectedDate) forControlEvents:UIControlEventValueChanged];
    _StartDate_DTC.inputView = datepicker_DTC;
    // _Enddate.inputView = datepicker;
    
    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,230,44)];
    [toolBar setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donePressed)];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space, doneBtn,nil]];
    [self.StartDate_DTC setInputAccessoryView:toolBar];
    
    
    
    _EndDate_DTC.delegate = self;
    datepicker1_DTC=[[UIDatePicker alloc] init];
    [datepicker1_DTC setMinimumDate:datepicker_DTC.date];
    [datepicker1_DTC setMaximumDate:now];
    datepicker1_DTC.datePickerMode=UIDatePickerModeDate;
    [datepicker1_DTC addTarget:self action:@selector(ShowSelectedDate1) forControlEvents:UIControlEventValueChanged];
    _EndDate_DTC.inputView = datepicker1_DTC;
    // _Enddate.inputView = datepicker;
    
    UIToolbar *toolBar1=[[UIToolbar alloc]initWithFrame:CGRectMake(0,0,230,44)];
    [toolBar1 setTintColor:[UIColor blackColor]];
    UIBarButtonItem *doneBtn1=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donePressed1)];
    UIBarButtonItem *space1=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar1 setItems:[NSArray arrayWithObjects:space1, doneBtn1,nil]];
    [self.EndDate_DTC setInputAccessoryView:toolBar1];
    // Do any additional setup after loading the view.
}

-(void)startDateSelected:(BOOL)done{
    NSString *str = [[CommanDataHandler sharedInstance] startDateSelected:datepicker_DTC.date endDate:datepicker1_DTC.date isdone:done];
    if (![str isEqualToString:@""]) {
        [datepicker1_DTC setMinimumDate:datepicker_DTC.date];
        self.StartDate_DTC.text=str;
        start_dtc = str;
    }else{
        datepicker_DTC.date = [CommanDataHandler getDateFromString:self.StartDate_DTC.text];
    }

}

-(void)endDateSelected:(BOOL)done{
    NSString* str= [[CommanDataHandler sharedInstance] endDateSelected:datepicker_DTC.date endDate:datepicker1_DTC.date isDone:done];
    if (![str isEqualToString:@""]) {
        [datepicker_DTC setMaximumDate:datepicker1_DTC.date];
        self.EndDate_DTC.text =str;
        end_dtc =str;
    }else{
        datepicker1_DTC.date=[CommanDataHandler getDateFromString:self.EndDate_DTC.text];
    }
}

- (void)ShowSelectedDate
{
    [self startDateSelected:false];
}
- (void)donePressed{
    [self startDateSelected:true];
    [self.view endEditing:true];
}

- (void)ShowSelectedDate1
{
    [self endDateSelected:false];
}
- (void)donePressed1{
    [self endDateSelected:true];
    [self.view endEditing:true];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
