//
//  SecondViewController.h
//  HotRodTV0
//
//  Created by Raghee Chandran M on 1/29/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import <UIKit/UIKit.h>
extern NSString *start_Track;
extern NSString *end_Track;
@interface SecondViewController : UIViewController <UITextFieldDelegate>
{
    
    UIDatePicker *datepicker_Track;
    UIDatePicker *datepicker1_Track;
    NSDateFormatter *formatter_Track;
    NSDateFormatter *formatter1_Track;
    
}
@property (weak, nonatomic) IBOutlet UITextField *StartDate_Track;

@property (weak, nonatomic) IBOutlet UITextField *EndDate_Track;

@end
