//
//  DTCDriverViewController.m
//  HotRodTV0
//
//  Created by Raghee Chandran M on 2/12/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import "DTCDriverViewController.h"
#import "FirstViewController.h"
#import "FifthViewController.h"
#import "ThirdViewController.h"


@interface DTCDriverViewController ()

@end
NSString *driver_dtc=@"";
int dr5=0;
@implementation DTCDriverViewController
@synthesize myArray;
@synthesize picker_dtc;
@synthesize myArray1;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //    _picker = [[UIPickerView alloc] init];
    _DTCContainer.hidden=YES;
    picker_dtc.hidden=YES;
    
    _Dname_Dtc.delegate = self;
    // NSLog(@"array%@",driverlist_cmn);
    myArray1 = [[NSMutableArray alloc]initWithArray:driver_name];
    _Dname_Dtc.text=[NSString stringWithFormat:@"%@",myArray1[0]];
    driver_dtc=[NSString stringWithFormat:@"%@",myArray1[0]];
    NSString *last=@"none";
    [myArray1 addObject:last];
    // NSLog(@"Array:%@",myArray1);
    size1=(int)[myArray1 count];
    // NSLog(@"%d",size1);   // _content_Track = [[NSArray alloc] initWithObjects:@"ALL",@"STOPS ONLY",@"IGNITION ON/OFF",@"MOVEMENTS ONLY",nil];
    // CatPicker.hidden = NO;
    picker_dtc.delegate=self;
    picker_dtc.dataSource=self;
    [_Dname_Dtc setInputView:picker_dtc];
    [picker_dtc reloadAllComponents];
    
    
    //self.myArray =[[NSArray alloc]initWithObjects:@"red", nil];
    
    // Do any additional setup after loading the view.
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    // _category.inputView=CatPicker;
    
    
    _DTCContainer.hidden = NO;
    
    picker_dtc.hidden = NO;
    
    
    
    
    
    
    
    return NO;
    
}




- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
    
    
}


//  returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return size1;
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return [myArray1 objectAtIndex:row];
    
    
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)componen1t{
    NSString *name = @"none";
    if (myArray1 != nil && myArray1.count >0) {
        name =[myArray1 objectAtIndex:row];
    }
    _Dname_Dtc.text = name;
    [self driverSelected];
    
}
-(void)driverSelected{
    NSString *driverName = _Dname_Dtc.text;
    if ([driverName  isEqualToString:@"none"]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Driver not exists-Please select another driver!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        _Dname_Dtc.text=driverName;
        driver_dtc = driverName;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)Done_dtc:(id)sender {
    [self driverSelected];
    _DTCContainer.hidden =YES;
}

- (IBAction)nextAction:(id)sender {
    NSString *name = _Dname_Dtc.text;
    if([name isEqualToString:@""] || [name isEqualToString:@"none"] ){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Driver not exists-Please select another driver!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else{
        UIStoryboard *storyBoard = self.storyboard;
        UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"DTCViewViewController"];
        [self.navigationController pushViewController:vc animated:true];
    }
}
@end
