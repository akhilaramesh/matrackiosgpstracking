//
//  FuelViewController.h
//  HotRodTV0
//
//  Created by Raghee Chandran M on 1/29/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import <UIKit/UIKit.h>
extern NSString *start_fuel;
extern NSString *end_fuel;
@interface FuelViewController : UIViewController <UITextFieldDelegate>
{
    
    UIDatePicker *datepicker_Fuel;
    UIDatePicker *datepicker1_Fuel;
    NSDateFormatter *formatter_Fuel;
    NSDateFormatter *formatter1_Fuel;
    
    
    
}
@property (weak, nonatomic) IBOutlet UITextField *StartDate_Fuel;

@property (weak, nonatomic) IBOutlet UITextField *EndDate_Fuel;

@end
