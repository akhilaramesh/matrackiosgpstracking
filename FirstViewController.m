//
//  FirstViewController.m
//  HotRodTV0
//
//  Created by Raghee Chandran M on 1/28/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import "FirstViewController.h"


@interface FirstViewController ()



@end
NSArray *data2;
NSString *master_flag=@"";
NSString *track_flag=@"";
NSString *report_flag=@"";
NSString *change_flag=@"";NSString *path=@"";
NSString *item=@"";
NSString *path1=@"";
NSString *usr=@"";
NSString *common_path=@"";
int x=0;
NSString *login_sucess=@"";
NSMutableArray *driver_name;
NSMutableArray *latitude;
NSMutableArray *longitude;
@implementation FirstViewController
@synthesize user;
@synthesize pass;
@synthesize failed_msg;
@synthesize submitBtn;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    failed_msg.hidden=YES;
       user.delegate=self;
    pass.delegate=self;
    NSUserDefaults *deafults = [NSUserDefaults standardUserDefaults];
    NSString *username1 = [deafults stringForKey:@"username"];
    if (([deafults objectForKey:@"username"]!=nil)&&([deafults objectForKey:@"password"]!=nil)){
        user.text = username1;
        pass.text = [deafults stringForKey:@"password"];
    }
    else{
        
        user.text = @"";
        pass.text = @"";
    }
    common_path = common_path1;
    path1 =[NSString stringWithFormat:@"%@/gpstracking",common_path];        // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

    UIEdgeInsets edge =  UIEdgeInsetsMake(bgScrollView.contentInset.top, bgScrollView.contentInset.left, 150.0f, bgScrollView.contentInset.right);
    [bgScrollView setContentInset:edge];
//    [self addNotification:bgScrollView];
}

//key board handle by kushal
-(void)keyboardWillShow:(NSNotification *)notification {
    [self adjustingHeight:YES notification:notification];

}

-(void) keyboardWillHide:(NSNotification *) notification {
    [self adjustingHeight:false notification:notification];
}

-(void) adjustingHeight:(BOOL )show notification:(NSNotification *)notification{
    NSDictionary *userInfo = notification.userInfo;
    //condition for only above iphone8.
    NSString *key = [[UIScreen mainScreen] bounds].size.height >= 812.0f ? UIKeyboardFrameBeginUserInfoKey:UIKeyboardFrameEndUserInfoKey;

    NSValue* keyboardFrameBegin = [userInfo valueForKey:key];
    CGRect keyboardFrame = [keyboardFrameBegin CGRectValue];
    float  changeInHeight = (keyboardFrame.size.height+40)*(show ?1:0);
    if (changeInHeight== 0)
        changeInHeight = 150.0f;

    UIEdgeInsets edge =  UIEdgeInsetsMake(bgScrollView.contentInset.top, bgScrollView.contentInset.left, changeInHeight, bgScrollView.contentInset.right);
    [bgScrollView setContentInset:edge];
    [bgScrollView setScrollIndicatorInsets:edge];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    NSLog(@"entered return key");
    return YES;
    
}
- (IBAction)SignIn:(id)sender {
    
    failed_msg.hidden=YES;
    NSString *Entered_User=[user text];
    NSString *Entered_Pass=[pass text];
    
    
    NSLog(@"server :%@",common_path);
    NSString *urlString = [NSString stringWithFormat:@"%@/EnableLogin_app2.php",path1];
    
    NSError *error;
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    /* NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
     @"IOS TYPE", @"typemap",
     nil];*/
    
    NSMutableDictionary *loginCredentials=[[NSMutableDictionary alloc]init];
    [loginCredentials setValue:Entered_User forKey:@"username"];
    [loginCredentials setValue:Entered_Pass forKey:@"password"];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:loginCredentials options:0 error:&error];
    [request setHTTPBody:postData];
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data !=nil) {
            NSDictionary *data1 =[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSLog(@"%@",data1);
            NSString *datastring=[NSString stringWithFormat:@"%@",data1];
            datastring = [datastring stringByReplacingOccurrencesOfString:@"(" withString:@""];
            datastring = [datastring stringByReplacingOccurrencesOfString:@")" withString:@""];
            datastring = [datastring stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            datastring = [datastring stringByReplacingOccurrencesOfString:@" " withString:@""];
            data2 = [datastring componentsSeparatedByString:@","];
            login_sucess = data2[0];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self moveToscreen];//ui event need main thread to update
            });
        }else if(error){
            NSString *val= [error.userInfo valueForKey:NSLocalizedDescriptionKey];
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:val delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            });
        }
    }];
    
    [postDataTask resume];

    
    


    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    [self redirect];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    [receivedData setLength:0];
    NSLog(@"This3 :%@",receivedData);
    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    [receivedData appendData:data];
    filePath = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    
    connectionSuccess = YES;
    //NSLog(@"%@",filePath);
    NSLog(@"success:%@",filePath);
    
    filePath = [filePath stringByReplacingOccurrencesOfString:@"[" withString:@""];
    filePath = [filePath stringByReplacingOccurrencesOfString:@"]" withString:@""];
    NSLog(@"%@",receivedData);
    _myArray1 = [filePath componentsSeparatedByString:@","];
    NSLog(@"arrat:%@",_myArray1[0]);
    login_sucess = _myArray1[0];
    NSLog(@"%@",login_sucess);
    if([login_sucess isEqualToString:@"false"]){
        
        UIStoryboard *storyBoard = self.storyboard;
        UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"loginscreen"];
        [self presentViewController:vc animated:YES completion:nil];
    }else{
        
        
        NSUserDefaults *userdata = [NSUserDefaults standardUserDefaults];
        [userdata setObject:[user text] forKey:@"username"];
        [userdata setObject:[pass text] forKey:@"password"];
        
        usr=_myArray1[1];
        usr=[usr stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        NSLog(@"%@%@",login_sucess,usr);
        /*int size= (int)[_myArray1 count];
         driver_name = [[NSMutableArray alloc] init];
         latitude = [[NSMutableArray alloc] init];
         longitude = [[NSMutableArray alloc] init];
         for(int i=2;i<size;i++){
         NSArray  *points = [[NSArray alloc]init];
         NSString *element = [[NSString alloc]init];
         element = _myArray1[i];
         element  = [element stringByReplacingOccurrencesOfString:@"\"" withString:@""];
         points = [element  componentsSeparatedByString:@"$"];
         NSLog(@"%@",points);
         [driver_name addObject:points[0]];
         [latitude addObject:points[1]];
         [longitude addObject:points[2]];
         // points_value[j] = points;
         //NSMutableArray *sam = [[NSMutableArray alloc] initWithArray:points];
         //[points_value addObject:sam];
         //points_value ins];
         
         }
         
         usr  = [usr stringByReplacingOccurrencesOfString:@"\"" withString:@""];*/
        NSString *path2 = [path1 stringByAppendingString:@"/client/"];
        
        NSString *path3 = [usr stringByAppendingString:@"/maps"];
        path = [path2 stringByAppendingString:path3];
        NSLog(@"new path:%@",path);
        
        NSLog(@"Final array:%@%@%@%@",driver_name,latitude,longitude,login_sucess);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self moveToscreen];//ui event need main thread to update
        });
    }
}





-(void)redirect{
    
    UIStoryboard *storyBoard = self.storyboard;
    UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"initial"];
    [self presentViewController:vc animated:YES completion:nil];
    // [timer invalidate];
    
    
}
- (void)moveToscreen{
    
    /* NSLog(@"login%@",login_sucess);
     login_sucess  = [login_sucess stringByReplacingOccurrencesOfString:@"\"" withString:@""];*/
    if ([login_sucess isEqualToString:@"1"]){
        NSLog(@"Entered");
        
        track_flag = data2[2];
        report_flag = data2[3];
        change_flag = data2[4];
        master_flag = data2[5];
        
        NSUserDefaults *userdata = [NSUserDefaults standardUserDefaults];
        [userdata setObject:user.text forKey:@"username"];
        [userdata setObject:pass.text forKey:@"password"];
        
        usr=data2[1];
         usr=[usr stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        
        NSString *path2 = [path1 stringByAppendingString:@"/client/"];
        
        NSString *path3 = [usr stringByAppendingString:@"/maps"];
        path = [path2 stringByAppendingString:path3];
        NSLog(@"new path:%@",path);
        dispatch_async(dispatch_get_main_queue(), ^{

        UIStoryboard *storyBoard = self.storyboard;
        UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"menuscreen"];
            [self presentViewController:vc animated:YES completion:nil];});
        
    }else{
        dispatch_async(dispatch_get_main_queue(), ^{
            failed_msg.hidden=NO;

            
        });
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
