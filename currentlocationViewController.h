//
//  currentlocationViewController.h
//  HotRodTV0
//
//  Created by Raghee Chandran M on 3/31/16.
//  Copyright © 2016 Sieva Networks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MapPin.h"
#import "FirstViewController.h"
#define MINIMUM_ZOOM_ARC 0.014
#define ANNOTATION_REGION_PAD_FACTOR 3.95
#define MAX_DEGREES_ARC 360
extern NSArray *final_addrs;
extern NSString *address2;
@interface currentlocationViewController : UIViewController<MKMapViewDelegate>{
    
    
    
    // 
    NSMutableData *receivedData;
    
    NSString *filePath;
    NSString *str;
    NSMutableString *addres1;
    //NSArray *myArray;
    
    BOOL connectionSuccess;
    
}
//@property (strong, nonatomic) IBOutlet UIButton *Menu;
@property (strong, nonatomic) NSArray *myArray1;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (strong, nonatomic) IBOutlet MKMapView *mymap;
@property (weak, nonatomic) IBOutlet UISegmentedControl *mycontrol;
- (IBAction)changeView:(id)sender;
- (IBAction)updateAgain:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *refresh;

@end
