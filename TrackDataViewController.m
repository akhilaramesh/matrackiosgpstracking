//
//  TrackDataViewController.m
//  HotRodTV0
//
//  Created by Raghee Chandran M on 2/11/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import "TrackDataViewController.h"
#import "FirstViewController.h"


@interface TrackDataViewController ()

@end
NSString *option_Track=@"";
NSMutableArray *driverlist;
NSString *str=@"";
int opt=0;
@implementation TrackDataViewController
@synthesize myArray;
@synthesize s;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _PickerViewContainer_Track.hidden = YES;
    
    _Picker_Track.hidden = YES;
    
    _Category_Track.delegate =self;
    _Category_Track.text=@"ALL";
    
    option_Track=@"ALL";
    _content_Track = [[NSArray alloc] initWithObjects:@"ALL",@"STOPS ONLY",@"IGNITION ON/OFF",@"MOVEMENTS ONLY",nil];
    // CatPicker.hidden = NO;
    _Picker_Track.delegate=self;
    _Picker_Track.dataSource=self;
    [_Category_Track setInputView:_Picker_Track];
    [_Picker_Track reloadAllComponents];
    // NSLog(@"testing--------------------->%d",opt);
    [_Picker_Track selectRow:0 inComponent:0 animated:YES];
    
    NSString *urlString = [NSString stringWithFormat:@"%@/driver_display.php",path];
    NSURLRequest *theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:(60.0)];
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    if (theConnection) {
        receivedData = [NSMutableData data];
        NSString *receivedDataString = [[NSString alloc]initWithData:receivedData encoding:NSUTF8StringEncoding];
        //NSLog(@"This1: %@",receivedData);
        
        NSLog(@"This2: %@",receivedDataString);
        
    }else
    {
        
        //NSLog(@"Connection Failed");
    }
    
    
    
    
    
    
    // Do any additional setup after loading the view.
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    [receivedData setLength:0];
    // NSLog(@"This3 :%@",receivedData);
    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    [receivedData appendData:data];
    filePath = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    
    connectionSuccess = YES;
    
    //NSLog(@"This4 :%@",[filePath stringByReplacingOccurrencesOfString:@"-" withString:@""]);
    //NSLog(@"This5 ---->test--->:%@",receivedData);
    //NSLog(@"%@",filePath);
    filePath = [filePath stringByReplacingOccurrencesOfString:@"[" withString:@""];
    filePath = [filePath stringByReplacingOccurrencesOfString:@"]" withString:@""];
    //NSLog(@"%@",filePath);
    self.myArray = [filePath componentsSeparatedByString:@","];
    // int sizea = [myArray count];
    //[self getData];
    driverlist = [[NSMutableArray alloc]init];
    for(s in myArray)
    {
        
        s = [s stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        [driverlist addObject:s];
        
    }
    //driverlist = [[NSMutableArray alloc]initWithArray:myArray];
    // NSLog(@"driverarray------------>%@",driverlist);
    
}



- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    // _category.inputView=CatPicker;
    
    
    _PickerViewContainer_Track.hidden = NO;
    
    _Picker_Track.hidden = NO;
    
    
    
    
    
    
    
    return NO;
    
}




- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
    
    
}


//  returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return 4;
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return [_content_Track objectAtIndex:row];
    
    
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    opt = opt +1;
    NSInteger row1=[_Picker_Track selectedRowInComponent:0];
    str = [_content_Track objectAtIndex:row1];
    
    //_output.text =str;
    //NSLog(@"selected%@",str);
    //NSLog(@"testing2-------->%d",opt);
    
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)Done_Track:(id)sender {
    if (opt == 0) {
        
        _Category_Track.text=[_content_Track objectAtIndex:0];
        option_Track =[_content_Track objectAtIndex:0];
        _PickerViewContainer_Track.hidden = YES;
    }
    else{
        
        _Category_Track.text=str;
        option_Track = str;
        _PickerViewContainer_Track.hidden = YES;
        
    }
    
}


@end
