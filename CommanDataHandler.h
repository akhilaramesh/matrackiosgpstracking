//
//  CommanDataHandler.h
//  MaTrack
//
//  Created by Kushal choudhari on 10/01/18.
//  Copyright © 2018 Sieva Networks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommanDataHandler : NSObject
+ (CommanDataHandler *)sharedInstance;
-(NSString*)startDateSelected:(NSDate*)startDate endDate:(NSDate*)end isdone:(BOOL)done;
-(NSString*)endDateSelected:(NSDate*)stratDate endDate:(NSDate*)endDate isDone:(BOOL)done;
+(NSDate*)getDateFromString:(NSString *)input;
-(BOOL)checkEndDate:(NSDate*)endDate IsGreaterThanOrEqualStartDate:(NSDate *)startDate;
@end
