//
//  GeofenceViewController.h
//  HotRodTV0
//
//  Created by Raghee Chandran M on 1/29/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//

#import <UIKit/UIKit.h>
extern NSString *driver_safe;

@interface GeofenceViewController : UIViewController <UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate>
{
    
    NSMutableData *receivedData;
    
    NSString *filePath;
    NSString *str;
    //NSArray *myArray;
    
    BOOL connectionSuccess;
    int size1;
}

@property (weak, nonatomic) IBOutlet UIPickerView *picker_safety;

@property (weak, nonatomic) IBOutlet UIView *SafetyContainer;

- (IBAction)DoneSafe:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *DriverSafe;
@property (strong, nonatomic) NSArray *myArray;
@property (strong, nonatomic) NSMutableArray *myArray1;
@end
