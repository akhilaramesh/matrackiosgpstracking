//
//  TrackDriverViewController.m
//  HotRodTV0
//
//  Created by Raghee Chandran M on 2/11/16.
//  Copyright (c) 2016 Sieva Networks. All rights reserved.
//
#import "TrackDriverViewController.h"
#import "FirstViewController.h"
#import "TrackDataViewController.h"

@interface TrackDriverViewController ()

@end
NSString *driver_Track=@"";
int dr=0;
@implementation TrackDriverViewController

@synthesize myArray;
@synthesize pickerdri_Track;
@synthesize myArray1;
@synthesize Driver_Track;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _PickerDriContainer_Track.hidden = YES;
    
    pickerdri_Track.hidden = YES;
    
    Driver_Track.delegate = self;
    // NSLog(@"array%@",driverlist);
    myArray1 = [[NSMutableArray alloc]initWithArray:driver_name];
    Driver_Track.text=[NSString stringWithFormat:@"%@",myArray1[0]];
    driver_Track=[NSString stringWithFormat:@"%@",myArray1[0]];
    NSString *last=@"none";
    [myArray1 addObject:last];
    // NSLog(@"Array:%@",myArray1);
    size1=(int)[myArray1 count];
    //NSLog(@"%d",size1);   // _content_Track = [[NSArray alloc] initWithObjects:@"ALL",@"STOPS ONLY",@"IGNITION ON/OFF",@"MOVEMENTS ONLY",nil];
    // CatPicker.hidden = NO;
    pickerdri_Track.delegate=self;
    pickerdri_Track.dataSource=self;
    [Driver_Track setInputView:pickerdri_Track];
    [pickerdri_Track reloadAllComponents];
    // NSLog(@"testing--------------------->%d",opt);
    // [pickerdri_Track selectRow:0 inComponent:0 animated:YES];
    
    
    //    _picker = [[UIPickerView alloc] init];
    
    
    /*  NSString *urlString = [NSString stringWithFormat:@"%@/driver_display.php",path];
     NSURLRequest *theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:(60.0)];
     NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
     if (theConnection) {
     receivedData = [NSMutableData data];
     NSString *receivedDataString = [[NSString alloc]initWithData:receivedData encoding:NSUTF8StringEncoding];
     NSLog(@"This1: %@",receivedData);
     
     NSLog(@"This2: %@",receivedDataString);
     
     }else
     {
     
     NSLog(@"Connection Failed");
     }*/
    
    //self.myArray =[[NSArray alloc]initWithObjects:@"red", nil];
    
    // Do any additional setup after loading the view.
}

/*- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
 
 [receivedData setLength:0];
 NSLog(@"This3 :%@",receivedData);
 
 
 }
 
 - (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
 
 [receivedData appendData:data];
 filePath = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
 
 connectionSuccess = YES;
 
 NSLog(@"This4 :%@",[filePath stringByReplacingOccurrencesOfString:@"-" withString:@""]);
 NSLog(@"This5 :%@",receivedData);
 NSLog(@"%@",filePath);
 filePath = [filePath stringByReplacingOccurrencesOfString:@"[" withString:@""];
 filePath = [filePath stringByReplacingOccurrencesOfString:@"]" withString:@""];
 NSLog(@"%@",filePath);
 self.myArray = [filePath componentsSeparatedByString:@","];
 
 [self getData];
 
 
 
 
 
 }
 - (void)getData
 {
 //self.myArray1=myArray;
 // picker =[[UIPickerView alloc]init];
 //_output.delegate=self;
 pickerdri_Track.delegate =self;
 pickerdri_Track.dataSource=self;
 _Driver_Track.inputView=pickerdri_Track;
 //[_picker setDelegate:self];
 // myArray1 =[[NSArray alloc]initWithObjects:@"red",@"black", nil];
 //  myArray1=myArray;
 myArray1 = [[NSMutableArray alloc]initWithArray:myArray];
 NSString *last=@"none";
 [myArray1 addObject:last];
 NSLog(@"Array:%@",myArray1);
 size1=[myArray1 count];
 NSLog(@"%d",size1);
 [pickerdri_Track reloadAllComponents];
 }*/


// returns the number of 'columns' to display.
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    _PickerDriContainer_Track.hidden = NO;
    pickerdri_Track.hidden = NO;
    return NO;
    
}




- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}


//  returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return size1;
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return [myArray1 objectAtIndex:row];
    
    
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
//    NSInteger row1=[pickerdri_Track selectedRowInComponent:0];
    NSString *name  = @"none";
    if (myArray1 != nil && myArray1.count>0) {
        name =[myArray1 objectAtIndex:row];
    }
    Driver_Track.text = name;
    [self driverSelected];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void)driverSelected{
    NSString *driverName = Driver_Track.text;
    if ([driverName  isEqualToString:@"none"]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Driver not exists-Please select another driver!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        Driver_Track.text=driverName;
        driver_Track = driverName;
    }
}

- (IBAction)DoneBtn_Track:(id)sender {
    [self driverSelected];
     _PickerDriContainer_Track.hidden = YES;
}
- (IBAction)submitAction:(id)sender {
    NSString *name = Driver_Track.text;
    if([name isEqualToString:@""] || [name isEqualToString:@"none"] ){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Attention" message:@"Driver not exists-Please select another driver!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else{
        UIStoryboard *storyBoard = self.storyboard;
        
        UIViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"trackHistoryViewController"];
        [self.navigationController pushViewController:vc animated:true];
    }

}


@end
